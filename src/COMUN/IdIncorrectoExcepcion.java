package COMUN;

public class IdIncorrectoExcepcion extends RuntimeException {
	
	//constructor sin parametros
	public IdIncorrectoExcepcion() {
		super();
	}

	//constructor con mensaje
	public IdIncorrectoExcepcion(String message) {
		super(message);
	}
	

}
