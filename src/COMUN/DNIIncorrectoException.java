package COMUN;

//esta clase pasa a formar parte de la jerarquia de Excepciones de Java
public class DNIIncorrectoException extends RuntimeException {

	//constructor sin parametros
	public DNIIncorrectoException() {
		super();
	}

	//constructor con mensaje
	public DNIIncorrectoException(String message) {
		super(message);
	}
	
	/*
	 *  OPERACIONES POSIBLES CON EXCEPCIONES:
	 * 
	 *  1. CREAR ---> class
	 *  2. GESTIONAR una excepcion que se genera en el programa ---> try / catch / finally
	 *  3. PROVOCAR LA EXCEPCION ---> throw
	 *  4. PROPAGAR LA EXCEPCION ---> throws
	 * 
	 */
}
