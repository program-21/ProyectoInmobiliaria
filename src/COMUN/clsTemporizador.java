package COMUN;

import java.sql.SQLException;

import LN.clsGestorLN;
/**
 * 
 * @author Asier Sojo e Ignacio Chapero--------- Industria 4.0
 *Clase temporizador que se encargará de generar un hilo simultáneo a la aplicación para que mediante un temporizador nos vuelque cada minuto
 *las viviendas de la lógica de negocio a la Base de Datos.
 */
public class clsTemporizador extends Thread 
{
	//tiempo total desde el inicio de la tarea
	private long tiempo = 0;

	//cada cuanto tiempo quieres la accion
	private int intervalo;
	
	/**
	 * 
	 * Esta clase simula un temporizador en la aplicacion
	 * 
	 * @param tiempo cada cuantos minutos quieres el temporizador
	 */
	public clsTemporizador( int intervalo ) {
	
		this.intervalo = intervalo;
	}
	
	public long getTiempo()
	{
		return tiempo;
	}
	
	@Override
	public void run() 
	{
		while ( true ) //bucle infinito en segundo plano
		{
			tiempo++;
			
			//hacer una pausa en esta tarea
			try {
				Thread.sleep(1000); //tic tac cada segundo, simular un segundo
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
														
			if ( tiempo  == 60 * intervalo )
			{
				System.out.println("Han pasado " + intervalo + " minutos!!!");
								
				/*
				 * 
				 *  PONER LA ACCION QUE QUIERES QUE SE EJECUTE!!!!!
				 *  
				 * 
				 */
				
				clsGestorLN.obtenerGestorLN().guardarLD();	
				System.out.println("guardando en BD");
				
				tiempo = 0;
			}			
		}
	}	
}
