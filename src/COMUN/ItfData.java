package COMUN;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import LD.BDException;
import LD.clsFila;


public interface ItfData {
	

	public void insertarUrbana(String idVivienda,String estado, int precio, int metros, int habitaciones, String disponibilidad,
			int precioAlquilerCatalogo, boolean trastero, boolean garaje)  throws BDException;
	public void actualizarUrbana(String idVivienda, String estado, int precio, int metros, int habitaciones, String disponibilidad,
			int precioAlquilerCatalogo, boolean trastero, boolean garaje);
	public void borrarUrbana(String idVivienda);

	public void insertarRural(String idVivienda, String estado, int precio, int metros, int habitaciones, String disponibilidad,
			int precioAlquilerCatalogo, int metrosTerreno, boolean granero)  throws BDException;
	public void actualizarRural(String idVivienda, String estado, int precio, int metros, int habitaciones, String disponibilidad,
			int precioAlquilerCatalogo, int metrosTerreno, boolean granero);
	public void borrarRural(String idVivienda);
	//==============================================================================================00
	public void borrarVendedor(String dni);
	public void insertarVendedorBD(String dni, String nombre, String email, String password, String idVendedor) throws BDException;
	public void actualizarVendedor(String dni, String nombre, String email, String password, String idVendedor);
	public void borrarCliente(String dni);
	public void insertarClienteBD(String dni, String nombre, String email, String password, String idCliente) throws BDException;
	public void actualizarCliente(String dni, String nombre, String email, String password, String idCliente);
	

}
