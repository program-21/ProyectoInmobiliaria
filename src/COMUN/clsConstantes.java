package COMUN;

public class clsConstantes {

	/**
	 * ----------------------------------------CONTSANTES---VIVIENDA-----------------------------------------
	 */
	public static final String ID_VIVIENDA = "idVivienda";
	public static final String ESTADO = "estado";
	public static final String PRECIO = "precio";
	public static final String METROS = "metros";
	public static final String HABITACIONES = "habitaciones";
	public static final String DISPONIBILIDAD = "disponibilidad";
	public static final String PRECIO_ALQUILER_CATALOGO = "pecioAlquilerCatalogo";

	/**
	 * ----------------------------------------CONSTANTES----RURAL---------------------------------------
	 */

	public static final String METROS_TERRENO = "metrosTerreno";
	public static final String GRANERO = "granero";

	/**
	 * ----------------------------------------------CONSTANTES------URBANAS---------------------------------
	 */

	public static final String TRASTERO = "trastero";
	public static final String GARAJE = "garaje";

	/**
	 * -------------------------------CONSTANTES----CONSULTAS-----PARAMETRIZADAS----BD-------------------
	 */

	public static final String RECUPERAR_URBANA = "select * from ViviendaUrbana where idViviendaUrbana = ?";
	public static final String RECUPERAR_RURAL = "select * from ViviendaRural where idViviendaRural = ?";
	public static final String INSERTAR_URBANA = "insert into ViviendaUrbana ( idViviendaUrbana, estado, precio, metros, habitaciones, disponibilidad, precioAlquilerCatalogo,\r\n"
			+ "	trastero, garaje ) values ( ?,?,?,?,?,?,?,?,?)";
	public static final String INSERTAR_RURAL = "insert into ViviendaRural ( idViviendaRural, estado, precio, metros, habitaciones, disponibilidad, precioAlquilerCatalogo,\r\n"
			+ "	metrosTerreno, granero ) values ( ?,?,?,?,?,?,?,?,?)";
	public static final String BORRAR_URBANA = "delete from ViviendaUrbana where idViviendaUrbana = ?";
	public static final String BORRAR_RURAL = "delete from ViviendaRural where idViviendaRural = ?";
	public static final String RECUPERAR_RURALES = "select * from ViviendaRural";
	public static final String RECUPERAR_URBANAS = "select * from ViviendaUrbana";
	public static final String ACTUALIZAR_RURAL = "UPDATE ViviendarURAL SET  estado = ?, precio = ?, metros = ?, habitaciones = ?, disponibilidad = ?, precioAlquilerCatalogo = ? ,  metrosTerreno = ?,granero = ?, WHERE idViviendaRural = ?";
	public static final String ACTUALIZAR_URBANA = "UPDATE ViviendaUrbana SET  estado = ?, precio = ?, metros = ?, habitaciones = ?, disponibilidad = ?, precioAlquilerCatalogo = ? ,  trastero = ?,garaje = ?, WHERE idViviendaUrbana = ?";
	//--------------------------------------------------------------------------------------------
	public static final String RECUPERAR_VENDEDOR = "select * from Vendedor where dni = ?";
	public static final String RECUPERAR_VENDEDORES = "select * from Vendedor";
	public static final String BORRAR_VENDEDOR = "delete from Vendedor where dni = ?";
	public static final String INSERTAR_VENDEDOR = "insert into Vendedor ( dni,  nombre,  email,  password,  idVendedor) values ( ?,?,?,?,?)";
	public static final String RECUPERAR_CLIENTE = "select * from Cliente where dni = ?";
	public static final String RECUPERAR_CLIENTES = "select * from Cliente";
	public static final String BORRAR_CLIENTE = "delete from Cliente where dni = ?";
	public static final String INSERTAR_CLIENTE = "insert into Cliente ( dni,  nombre,  email,  password,  idCliente) values ( ?,?,?,?,?)";
	
	/**
	 * ------------------------------CONSTANTES------PERSONA---------------------------------------------------------
	 */
	
	public static final String DNI = "dni";
	public static final String NOMBRE = "nombre";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	//---------------------------------CLIENTES---------------------------------------
	public static final String ID_CLIENTE = "idCliente";
	//---------------------------------VENDEDORES---------------------------------------
	public static final String ID_VENDEDOR = "idVendedor";
	//-----------------------------------------------------------------------------------
	public static final String CONSULTAR_NOMBRE_PASSWORD_VENDEDOR = "select nombre,password from vendedor where  nombre = ? AND password = ?";
	public static final String CONSULTAR_NOMBRE_PASSWORD_CLIENTE = "select nombre,password from cliente where  nombre = ? AND password = ?";
	
	
	
	
	

}
