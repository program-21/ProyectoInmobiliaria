
import java.awt.EventQueue;

import LN.clsGestorLN;
import LP.VentanaLogin;

/**
 * Clase Principal del programa que ejecuta la ventana de Login cuando
 * arrancamos la App
 * 
 * @author Asier y Natxo
 *
 */
public class clsMain {
	public static void main(String[] args) {
		try {
			VentanaLogin dialog = new VentanaLogin();

			// dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
