package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import LN.clsGestorLN;

/**
 * Ventana que se encarga de gestionar la eliminación de las viviendascurbanas
 * de base de datos
 * 
 * 
 * @author Asier y Natxo
 *
 */
public class VentanaBorrarUrbana extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField IDaBorrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VentanaMostrarUrbana dialog = new VentanaMostrarUrbana();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaBorrarUrbana() {
		setTitle("Borrar Viviendas Urbanas");
		setBounds(100, 100, 900, 571);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(107, 35, 750, 453);
		contentPanel.add(scrollPane);

		TextArea textArea = new TextArea();
		textArea.setFont(new Font("Courier New", Font.PLAIN, 10));
		scrollPane.setViewportView(textArea);

		JButton btnTodas = new JButton("MostrarViviendas");
		btnTodas.setBounds(10, 38, 89, 23);
		contentPanel.add(btnTodas);

		btnTodas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().recuperarDatosUrbanas();

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tTrastero \tGaraje\n";
				textArea.append(cabecera);
				textArea.append(
						"==============================================================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String idVivienda = (String) type.getObjectProperty(clsConstantes.ID_VIVIENDA);
					String estado = (String) type.getObjectProperty(clsConstantes.ESTADO);
					int precio = (int) type.getObjectProperty(clsConstantes.PRECIO);
					int metros = (int) type.getObjectProperty(clsConstantes.METROS);
					int habitaciones = (int) type.getObjectProperty(clsConstantes.HABITACIONES);
					String disponibilidad = (String) type.getObjectProperty(clsConstantes.DISPONIBILIDAD);
					int precioAlquilerCatalogo = (int) type.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);
					Boolean trastero = (Boolean) type.getObjectProperty(clsConstantes.TRASTERO);
					Boolean garaje = (Boolean) type.getObjectProperty(clsConstantes.GARAJE);

					textArea.append("" + idVivienda);
					textArea.append("\t\t" + estado);
					textArea.append("\t" + precio);
					textArea.append("\t" + metros);
					textArea.append("\t" + habitaciones);
					textArea.append("\t\t" + disponibilidad);
					textArea.append("\t\t" + precioAlquilerCatalogo);
					textArea.append("\t\t\t" + trastero);
					textArea.append("\t" + garaje);
					textArea.append("\n");
					textArea.append(
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

				}
			}
		});

		IDaBorrar = new JTextField();
		IDaBorrar.setBounds(12, 145, 85, 23);
		contentPanel.add(IDaBorrar);
		IDaBorrar.setColumns(10);

		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String idBorrar = IDaBorrar.getText();

				clsGestorLN.obtenerGestorLN().borrarUrbana(idBorrar);

				IDaBorrar.setText("");

// -------------------------------------------Para recargar las viviendas urabanas mostradas-----------------------------------------------------------------

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().recuperarDatosUrbanas();

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tTrastero \tGaraje\n";
				textArea.append(cabecera);
				textArea.append(
						"==============================================================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String idVivienda = (String) type.getObjectProperty(clsConstantes.ID_VIVIENDA);
					String estado = (String) type.getObjectProperty(clsConstantes.ESTADO);
					int precio = (int) type.getObjectProperty(clsConstantes.PRECIO);
					int metros = (int) type.getObjectProperty(clsConstantes.METROS);
					int habitaciones = (int) type.getObjectProperty(clsConstantes.HABITACIONES);
					String disponibilidad = (String) type.getObjectProperty(clsConstantes.DISPONIBILIDAD);
					int precioAlquilerCatalogo = (int) type.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);
					Boolean trastero = (Boolean) type.getObjectProperty(clsConstantes.TRASTERO);
					Boolean garaje = (Boolean) type.getObjectProperty(clsConstantes.GARAJE);

					textArea.append("" + idVivienda);
					textArea.append("\t\t" + estado);
					textArea.append("\t" + precio);
					textArea.append("\t" + metros);
					textArea.append("\t" + habitaciones);
					textArea.append("\t\t" + disponibilidad);
					textArea.append("\t\t" + precioAlquilerCatalogo);
					textArea.append("\t\t\t" + trastero);
					textArea.append("\t" + garaje);
					textArea.append("\n");
					textArea.append(
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

				}
// ----------------------------------------------------------------------------------------------------------------------------------------------------
			}
		});
		btnBorrar.setBounds(10, 187, 89, 23);
		contentPanel.add(btnBorrar);

	}
}
