package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import LN.clsGestorLN;

/**
 * Ventana que se encarga de gestionar la visualización de todas las viviendas
 * rurales, almacenadas en BD, en tablas por medio de ordenación o busqueda de
 * una de ellas por ID
 * 
 *
 * @author Asier y Natxo
 *
 */
public class VentanaMostrarRural extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField IDaBuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VentanaMostrarRural dialog = new VentanaMostrarRural();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaMostrarRural() {
		setTitle("Consultar Viviendas Rurales");
		setBounds(100, 100, 900, 571);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(107, 35, 750, 453);
		contentPanel.add(scrollPane);

		TextArea textArea = new TextArea();
		textArea.setFont(new Font("Courier New", Font.PLAIN, 10));
		scrollPane.setViewportView(textArea);

		JButton btnOrdenadas = new JButton("Ordenadas por Precio");
		btnOrdenadas.setBounds(147, 11, 176, 23);
		contentPanel.add(btnOrdenadas);

		btnOrdenadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().mostrarRuralesPrecioAscendente().iterator();

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tMetrosTerreno \tGranero\n";
				textArea.append(cabecera);
				textArea.append(
						"=======================================================================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String idVivienda = (String) type.getObjectProperty(clsConstantes.ID_VIVIENDA);
					String estado = (String) type.getObjectProperty(clsConstantes.ESTADO);
					int precio = (int) type.getObjectProperty(clsConstantes.PRECIO);
					int metros = (int) type.getObjectProperty(clsConstantes.METROS);
					int habitaciones = (int) type.getObjectProperty(clsConstantes.HABITACIONES);
					String disponibilidad = (String) type.getObjectProperty(clsConstantes.DISPONIBILIDAD);
					int precioAlquilerCatalogo = (int) type.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);
					int metrosTerreno = (int) type.getObjectProperty(clsConstantes.METROS_TERRENO);
					Boolean granero = (Boolean) type.getObjectProperty(clsConstantes.GRANERO);

					textArea.append("" + idVivienda);
					textArea.append("\t\t" + estado);
					textArea.append("\t" + precio);
					textArea.append("\t" + metros);
					textArea.append("\t" + habitaciones);
					textArea.append("\t\t" + disponibilidad);
					textArea.append("\t\t" + precioAlquilerCatalogo);
					textArea.append("\t\t\t" + metrosTerreno);
					textArea.append("\t\t" + granero);
					textArea.append("\n");
					textArea.append(
							"-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

				}
			}
		});

		IDaBuscar = new JTextField();
		IDaBuscar.setBounds(10, 68, 85, 23);
		contentPanel.add(IDaBuscar);
		IDaBuscar.setColumns(10);

		JButton btnPorid = new JButton("PorID");
		btnPorid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textArea.setText(" ");

				String id = IDaBuscar.getText();

				itfProperty x = clsGestorLN.obtenerGestorLN().recuperarRural(id);

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tMetrosTerreno \tGranero\n";
				textArea.append(cabecera);
				textArea.append(
						"=======================================================================================================================\n");

				textArea.append((String) "" + x.getObjectProperty(clsConstantes.ID_VIVIENDA));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.ESTADO));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.PRECIO));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.METROS));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.HABITACIONES));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.DISPONIBILIDAD));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO));
				textArea.append((String) "\t\t\t" + x.getObjectProperty(clsConstantes.METROS_TERRENO));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.GRANERO));
				textArea.append("\n");

				IDaBuscar.setText("");

			}
		});
		btnPorid.setBounds(8, 102, 89, 23);
		contentPanel.add(btnPorid);

		JButton btnTodas = new JButton("Todas");
		btnTodas.setBounds(12, 11, 89, 23);
		contentPanel.add(btnTodas);

		btnTodas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().recuperarDatosRurales();

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tMetrosTerreno \tGranero\n";
				textArea.append(cabecera);
				textArea.append(
						"=======================================================================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String idVivienda = (String) type.getObjectProperty(clsConstantes.ID_VIVIENDA);
					String estado = (String) type.getObjectProperty(clsConstantes.ESTADO);
					int precio = (int) type.getObjectProperty(clsConstantes.PRECIO);
					int metros = (int) type.getObjectProperty(clsConstantes.METROS);
					int habitaciones = (int) type.getObjectProperty(clsConstantes.HABITACIONES);
					String disponibilidad = (String) type.getObjectProperty(clsConstantes.DISPONIBILIDAD);
					int precioAlquilerCatalogo = (int) type.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);
					int metrosTerreno = (int) type.getObjectProperty(clsConstantes.METROS_TERRENO);
					Boolean granero = (Boolean) type.getObjectProperty(clsConstantes.GRANERO);

					textArea.append("" + idVivienda);
					textArea.append("\t\t" + estado);
					textArea.append("\t" + precio);
					textArea.append("\t" + metros);
					textArea.append("\t" + habitaciones);
					textArea.append("\t\t" + disponibilidad);
					textArea.append("\t\t" + precioAlquilerCatalogo);
					textArea.append("\t\t\t" + metrosTerreno);
					textArea.append("\t\t" + granero);
					textArea.append("\n");
					textArea.append(
							"-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

				}
			}
		});

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("OK");
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}

	}
}
