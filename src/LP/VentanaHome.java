package LP;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;

import COMUN.clsConstantes;
import COMUN.clsTemporizador;
import COMUN.itfProperty;
import LN.clsEstadisticaViviendas;
import LN.clsGestorLN;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.UIManager;

//LA VENTANA JEFA!
public class VentanaHome extends JFrame {

	private clsTemporizador temporizador = new clsTemporizador(1);

	private JPanel contentPane;

	private JMenuItem mntmInsertar;

	private JMenuItem mntmBorrar;

	private JMenuItem mntmBorrar_1;

	private JMenuItem mntmInsertar_1;

	private JLabel precioM2U;

	private JLabel precioM2R;

	/**
	 * Crea el frame de la ventana principal de la App
	 * 
	 * Cuando Arranca la App trae todos los datos de la BD hasta la LN.
	 *  
	 * En la cual se arranca un hilo de ejecución paralelo que se encargará de relizar un back up 
	 * de los datos de la App en BD. 
	 */
	public VentanaHome(String usuario) {

		setBackground(Color.LIGHT_GRAY);

		temporizador.start();

		// con estas tres lineas cargamos en la LN todas las viviendas de BD y los clientes mientras
		// esta arrancando la ventana Home

		clsGestorLN.obtenerGestorLN().recuperarViviendasUrbanasBD();
		clsGestorLN.obtenerGestorLN().recuperarViviendasRuralesBD();
		clsGestorLN.obtenerGestorLN().recuperarClientes();

		setTitle("Home - Accediendo como " + usuario);
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Urbana.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(100, 100, 884, 447);

		setLocationRelativeTo(null);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnVivienda = new JMenu("Vivienda");
		menuBar.add(mnVivienda);

		JMenu mnViviendaUrbana = new JMenu("Vivienda Urbana");
		mnVivienda.add(mnViviendaUrbana);

		mntmInsertar = new JMenuItem("Insertar");
		mntmInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("insetar vivienda urbana");

				VentanaInsertaUrbana viu = new VentanaInsertaUrbana();

				viu.setVisible(true);
			}
		});
		mnViviendaUrbana.add(mntmInsertar);

		mntmBorrar = new JMenuItem("Borrar");
		mntmBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Borrar vivienda urbana");

				VentanaBorrarUrbana vbu = new VentanaBorrarUrbana();

				vbu.setVisible(true);

			}
		});

		JMenuItem mntmMostrar = new JMenuItem("Mostrar");
		mntmMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Mostrar vivienda urbana");

				VentanaMostrarUrbana vmu = new VentanaMostrarUrbana();

				vmu.setVisible(true);

			}
		});
		mnViviendaUrbana.add(mntmMostrar);
		mnViviendaUrbana.add(mntmBorrar);

		JMenu mnVviendaRural = new JMenu("Vivienda Rural");
		mnVivienda.add(mnVviendaRural);

		mntmInsertar_1 = new JMenuItem("Insertar");
		mntmInsertar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Insetar vivienda Rural");

				VentanaInsertaRural vir = new VentanaInsertaRural();

				vir.setVisible(true);

			}
		});
		mnVviendaRural.add(mntmInsertar_1);

		mntmBorrar_1 = new JMenuItem("Borrar");
		mntmBorrar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("Borrar Rural");

				VentanaBorrarRural vbr = new VentanaBorrarRural();

				vbr.setVisible(true);

			}
		});

		JMenuItem mntmMostrar1 = new JMenuItem("Mostrar");
		mntmMostrar1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				System.out.println("Mostrar vivienda Rural");

				VentanaMostrarRural vmr = new VentanaMostrarRural();

				vmr.setVisible(true);

			}

		});
		mnVviendaRural.add(mntmMostrar1);
		mnVviendaRural.add(mntmBorrar_1);

		JMenu mnNewMenuClientes = new JMenu("Clientes");
		menuBar.add(mnNewMenuClientes);

		JMenuItem mntmBuscarClientes = new JMenuItem("Buscar");
		mntmBuscarClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				System.out.println("Mostrar Clientes");

				VentanaMostrarClientes vmc = new VentanaMostrarClientes();

				vmc.setVisible(true);
			}
		});
		mnNewMenuClientes.add(mntmBuscarClientes);

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblNewLabel = new JLabel("New label");

		JLabel lblPrecioMetroCuadrado = new JLabel("Precio del Metro Cuadrado de Nuestras Viviendas a Tiempo Real");
		lblPrecioMetroCuadrado.setFont(new Font("Source Sans Pro Black", Font.PLAIN, 23));

		JButton btnViviendasUrbanas = new JButton("Viviendas Urbanas");
		btnViviendasUrbanas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				clsEstadisticaViviendas ostiadistica = new clsEstadisticaViviendas();
				ostiadistica.calcularPrecioMedioUrbanaMC();
				int x = ostiadistica.getPrecioMedioUrbanaMC();
				precioM2U.setText("");
				precioM2R.setText("");
				precioM2U.setText("" + x);
			}
		});

		JButton btnViviendasRurales = new JButton("Viviendas Rurales");
		btnViviendasRurales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				clsEstadisticaViviendas estadistica = new clsEstadisticaViviendas();
				estadistica.calcularPrecioMedioRuralMC();
				int y = estadistica.getPrecioMedioRuralMC();
				precioM2R.setText("");
				precioM2U.setText("");
				precioM2R.setText("" + y);
			}
		});

		JLabel lblNewLabel_2 = new JLabel("\r\n");
		lblNewLabel_2.setIcon(new ImageIcon(VentanaHome.class.getResource("/img/chale_a_color_med.jpg")));

		if (usuario.equalsIgnoreCase("admin") == false) {
			mntmInsertar.setEnabled(false);
			mntmBorrar.setEnabled(false);
			mntmBorrar_1.setEnabled(false);
			mntmInsertar_1.setEnabled(false);
			mntmBuscarClientes.setEnabled(false);
		}

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(VentanaHome.class.getResource("/img/pis_colo_tras_dime.png")));

		precioM2U = new JLabel("");
		precioM2U.setFont(new Font("Playbill", Font.PLAIN, 25));

		precioM2R = new JLabel("");
		precioM2R.setFont(new Font("Playbill", Font.PLAIN, 25));

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup()
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup().addGap(256).addComponent(btnViviendasUrbanas))
						.addGroup(gl_contentPane.createSequentialGroup().addGap(742).addComponent(lblNewLabel,
								GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(111, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup().addContainerGap(241, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 156,
										GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(precioM2U, GroupLayout.PREFERRED_SIZE, 89,
												GroupLayout.PREFERRED_SIZE)
										.addGap(16)))
						.addGap(58)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup().addGap(10)
										.addComponent(btnViviendasRurales))
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
										.addComponent(precioM2R, GroupLayout.PREFERRED_SIZE, 112,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 155,
												GroupLayout.PREFERRED_SIZE)))
						.addGap(248))
				.addGroup(gl_contentPane.createSequentialGroup().addGap(62).addComponent(lblPrecioMetroCuadrado)
						.addContainerGap(140, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup()
				.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE).addGap(66)
				.addComponent(lblPrecioMetroCuadrado, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
				.addGap(32)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(precioM2U, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(precioM2R, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addComponent(lblNewLabel_2)
						.addComponent(lblNewLabel_1))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(btnViviendasUrbanas)
						.addComponent(btnViviendasRurales))
				.addGap(140)));
		contentPane.setLayout(gl_contentPane);
	}
}
