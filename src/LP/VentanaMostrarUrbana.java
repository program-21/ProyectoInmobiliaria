package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import LN.clsGestorLN;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.TextArea;

/**
 * Ventana que se encarga de gestionar la visualización de todas las viviendas
 * urbanas, almacenadas en BD, en tablas por medio de ordenación o busqueda de
 * una de ellas por ID
 * 
 *
 * @author Asier y Natxo
 *
 */
public class VentanaMostrarUrbana extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField IDaBuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VentanaMostrarUrbana dialog = new VentanaMostrarUrbana();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaMostrarUrbana() {
		setTitle("Consultar Viviendas Urbanas");
		setBounds(100, 100, 900, 571);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(107, 35, 750, 453);
		contentPanel.add(scrollPane);

		TextArea textArea = new TextArea();
		textArea.setFont(new Font("Courier New", Font.PLAIN, 10));
		scrollPane.setViewportView(textArea);

		JButton btnOrdenadas = new JButton("Ordenadas por Precio");
		btnOrdenadas.setBounds(128, 11, 160, 23);
		contentPanel.add(btnOrdenadas);

		btnOrdenadas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().mostrarUrbanasPrecioAscendente().iterator();

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tTrastero \tGaraje\n";
				textArea.append(cabecera);
				textArea.append(
						"==============================================================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String idVivienda = (String) type.getObjectProperty(clsConstantes.ID_VIVIENDA);
					String estado = (String) type.getObjectProperty(clsConstantes.ESTADO);
					int precio = (int) type.getObjectProperty(clsConstantes.PRECIO);
					int metros = (int) type.getObjectProperty(clsConstantes.METROS);
					int habitaciones = (int) type.getObjectProperty(clsConstantes.HABITACIONES);
					String disponibilidad = (String) type.getObjectProperty(clsConstantes.DISPONIBILIDAD);
					int precioAlquilerCatalogo = (int) type.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);
					Boolean trastero = (Boolean) type.getObjectProperty(clsConstantes.TRASTERO);
					Boolean garaje = (Boolean) type.getObjectProperty(clsConstantes.GARAJE);

					textArea.append("" + idVivienda);
					textArea.append("\t\t" + estado);
					textArea.append("\t" + precio);
					textArea.append("\t" + metros);
					textArea.append("\t" + habitaciones);
					textArea.append("\t\t" + disponibilidad);
					textArea.append("\t\t" + precioAlquilerCatalogo);
					textArea.append("\t\t\t" + trastero);
					textArea.append("\t" + garaje);
					textArea.append("\n");
					textArea.append(
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

				}
			}
		});

		IDaBuscar = new JTextField();
		IDaBuscar.setBounds(12, 66, 85, 23);
		contentPanel.add(IDaBuscar);
		IDaBuscar.setColumns(10);

		JButton btnPorid = new JButton("PorID");
		btnPorid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textArea.setText(" ");

				String id = IDaBuscar.getText();

				itfProperty x = clsGestorLN.obtenerGestorLN().recuperarUrbana(id);

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tTrastero \tGaraje\n";
				textArea.append(cabecera);
				textArea.append(
						"==============================================================================================================\n");

				textArea.append((String) "" + x.getObjectProperty(clsConstantes.ID_VIVIENDA));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.ESTADO));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.PRECIO));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.METROS));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.HABITACIONES));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.DISPONIBILIDAD));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO));
				textArea.append((String) "\t\t\t" + x.getObjectProperty(clsConstantes.TRASTERO));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.GARAJE));
				textArea.append("\n");

				IDaBuscar.setText("");

			}
		});
		btnPorid.setBounds(12, 100, 85, 23);
		contentPanel.add(btnPorid);

		JButton btnTodas = new JButton("Todas");
		btnTodas.setBounds(12, 11, 89, 23);
		contentPanel.add(btnTodas);

		btnTodas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().recuperarDatosUrbanas();

				textArea.setText(" ");
				String cabecera = "idVivienda \tEstado \tPrecio \tMetros \tHabitaciones \tDisponibilidad \tPrecioAlquilerCatalogo \tTrastero \tGaraje\n";
				textArea.append(cabecera);
				textArea.append(
						"==============================================================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String idVivienda = (String) type.getObjectProperty(clsConstantes.ID_VIVIENDA);
					String estado = (String) type.getObjectProperty(clsConstantes.ESTADO);
					int precio = (int) type.getObjectProperty(clsConstantes.PRECIO);
					int metros = (int) type.getObjectProperty(clsConstantes.METROS);
					int habitaciones = (int) type.getObjectProperty(clsConstantes.HABITACIONES);
					String disponibilidad = (String) type.getObjectProperty(clsConstantes.DISPONIBILIDAD);
					int precioAlquilerCatalogo = (int) type.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);
					Boolean trastero = (Boolean) type.getObjectProperty(clsConstantes.TRASTERO);
					Boolean garaje = (Boolean) type.getObjectProperty(clsConstantes.GARAJE);

					textArea.append("" + idVivienda);
					textArea.append("\t\t" + estado);
					textArea.append("\t" + precio);
					textArea.append("\t" + metros);
					textArea.append("\t" + habitaciones);
					textArea.append("\t\t" + disponibilidad);
					textArea.append("\t\t" + precioAlquilerCatalogo);
					textArea.append("\t\t\t" + trastero);
					textArea.append("\t" + garaje);
					textArea.append("\n");
					textArea.append(
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

				}
			}
		});

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("OK");
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}

	}
}
