package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import COMUN.DNIIncorrectoException;
import LD.clsGestorLD;
import LN.clsGestorLN;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Ventana encargada de gestionar el registro de los nuevos clientes para poder
 * acceder a la aplicación
 * 
 * @author Asier y Natxo
 *
 */
public class VentanaRegistro extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtDni;
	private JTextField txtNombre;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textEmail;
	private JTextField textPassword;
	private JTextField textId;

	/**
	 * Create the dialog.
	 */
	public VentanaRegistro() {

		setTitle("REGISTRO NUEVO CLIENTE");

		setModal(true);

		setBounds(100, 100, 388, 406);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.WEST);

		JLabel lblNewLabel = new JLabel("DNI :");

		txtDni = new JTextField();
		txtDni.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Nombre :");

		txtNombre = new JTextField();
		txtNombre.setColumns(10);

		JLabel lblEmail = new JLabel("Email :");

		textEmail = new JTextField();
		textEmail.setColumns(10);

		JLabel lblPassword = new JLabel("Password:");

		textPassword = new JTextField();
		textPassword.setColumns(10);

		JLabel lblId = new JLabel("ID :");

		textId = new JTextField();
		textId.setColumns(10);

		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup().addContainerGap().addGroup(gl_contentPanel
						.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_contentPanel.createSequentialGroup().addComponent(lblNewLabel)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(txtDni, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup().addComponent(lblNewLabel_1).addGap(18)
										.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPanel.createSequentialGroup()
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING).addComponent(lblEmail)
										.addComponent(lblId, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblPassword, GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
										.addComponent(textEmail, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
										.addComponent(textId, 0, 0, Short.MAX_VALUE)
										.addComponent(textPassword, 0, 0, Short.MAX_VALUE))))
						.addContainerGap()));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPanel
				.createSequentialGroup()
				.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup().addContainerGap()
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblNewLabel).addComponent(txtDni, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblNewLabel_1).addComponent(txtNombre, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(54)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING, false)
										.addGroup(gl_contentPanel.createSequentialGroup().addComponent(lblEmail)
												.addGap(22).addComponent(lblPassword).addGap(34).addComponent(lblId))
										.addGroup(gl_contentPanel.createSequentialGroup().addGap(34)
												.addComponent(textPassword, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(textId, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGroup(gl_contentPanel.createSequentialGroup().addGap(120).addComponent(textEmail,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(93, Short.MAX_VALUE)));
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Guardar");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						// if(rdbtnCliente.isSelected()) {

						String dni = txtDni.getText();
						String nombre = txtNombre.getText();
						String email = textEmail.getText();
						String password = textPassword.getText();
						String id = textId.getText();
						// clsGestorLD NO!!!!

						try {
							clsGestorLN.obtenerGestorLN().insertaCliente(dni, nombre, email, password, id);
							clsGestorLN.obtenerGestorLN().guardarClienteLD();
						} catch (DNIIncorrectoException e) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(null, "LA LETRA DEDE SER MAYUSCULA");
						}

						// }
						/*
						 * else if(rdbtnVendedor.isSelected()) {
						 * 
						 * String dni = txtDni.getText(); String nombre = txtNombre.getText(); String
						 * email = textEmail.getText(); String password = textPassword.getText(); String
						 * id = textId.getText(); //clsGestorLD NO!!!!
						 * 
						 * try { clsGestorLN.obtenerGestorLN().insertaVendedor(dni, nombre, email,
						 * password,id); clsGestorLN.obtenerGestorLN().guardarVendedorLD(); } catch
						 * (DNIIncorrectoException e) { // TODO Auto-generated catch block
						 * JOptionPane.showMessageDialog(null, "LA LETRA DEDE SER MAYUSCULA"); }
						 * 
						 * }
						 */

						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}
