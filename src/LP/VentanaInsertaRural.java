package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.ImageIcon;
/**
 * Ventana que se encarga de gestionar la  inserción de viviendas rurales en LN
 * para su posterior bolcado a base de datos
 * 
 * 
 * @author Asier y Natxo
 *
 */
public class VentanaInsertaRural extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField_IdViviendaRural;
	private JTextField textField_PrecioRural;
	private JTextField textField_DisponibilidadRural;
	private JTextField textField_HabitacionesRural;
	private JTextField textField_MetrosRural;
	private JTextField textField_MetrosTerrenoRural;
	private JTextField textField_PrecioAlquilerRural;
	private JRadioButton rdbtnAlquiler;
	private JRadioButton rdbtnVenta;
	private JRadioButton rdbtnGraneroSi;
	private JRadioButton rdbtnGraneroNo;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VentanaInsertaRural dialog = new VentanaInsertaRural();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaInsertaRural() {
		setTitle("INSERTAR VIVIENDA RURAL");
		setBounds(100, 100, 502, 355);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane);
			buttonPane.setLayout(null);
			contentPanel.setBounds(303, 5, 56, 24);
			buttonPane.add(contentPanel);
			contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPanel.setLayout(new BorderLayout(0, 0));
			{
				JButton GuardarButton = new JButton("Guardar");
				GuardarButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						

						String idVivienda = textField_IdViviendaRural.getText();
						
						String estado ="";
						
						if (rdbtnVenta.isSelected()) {
							  estado = "venta";
						}else if (rdbtnAlquiler.isSelected()) {
							estado = "alquiler";
						}
						
						int precio = Integer.parseInt(textField_PrecioRural.getText());
						int metros = Integer.parseInt(textField_MetrosRural.getText());
						int habitaciones = Integer.parseInt(textField_HabitacionesRural.getText());
						String disponibilidad = textField_DisponibilidadRural.getText();
						int precioAlquiler = Integer.parseInt(textField_PrecioAlquilerRural.getText());
						int metrosTerreno = Integer.parseInt(textField_MetrosTerrenoRural.getText());
						
						
						Boolean granero = null;

						if (rdbtnGraneroSi.isSelected()) {
							granero = true;
						} else if (rdbtnGraneroNo.isSelected()) {
							granero = false;
						}
						
						boolean r = clsGestorLN.obtenerGestorLN().insertarRural(idVivienda, estado, precio, metros, habitaciones,
								disponibilidad, precioAlquiler, metrosTerreno, granero);
						
						if (r == false) {
							
							JOptionPane.showMessageDialog(null, "Esta vivienda ya esta registrada");
						}


						

						/**
						 * Para vaciar los campos de la vivienda tras introducirla
						 */
						textField_IdViviendaRural.setText("");
						textField_PrecioRural.setText("");
						textField_MetrosRural.setText("");
						textField_HabitacionesRural.setText("");
						textField_DisponibilidadRural.setText("");
						textField_PrecioAlquilerRural.setText("");
						textField_MetrosTerrenoRural.setText("");

					}
				});
				GuardarButton.setBounds(395, 282, 81, 23);
				GuardarButton.setActionCommand("Guardar");
				buttonPane.add(GuardarButton);
				getRootPane().setDefaultButton(GuardarButton);
			}
			{
				JLabel lblNewLabel = new JLabel("Estado");
				lblNewLabel.setBounds(10, 52, 81, 14);
				buttonPane.add(lblNewLabel);
			}
			{
				JLabel lblNewLabel_1 = new JLabel("Precio");
				lblNewLabel_1.setBounds(10, 85, 81, 14);
				buttonPane.add(lblNewLabel_1);
			}

			JLabel lblMetros = new JLabel("Metros");
			lblMetros.setBounds(10, 118, 81, 14);
			buttonPane.add(lblMetros);

			JLabel lblHabitaciones = new JLabel("Habitaciones");
			lblHabitaciones.setBounds(10, 151, 81, 14);
			buttonPane.add(lblHabitaciones);

			JLabel lblDisponibilidad = new JLabel("Disponibilidad");
			lblDisponibilidad.setBounds(10, 184, 81, 14);
			buttonPane.add(lblDisponibilidad);

			JLabel lblPrecioAlquilerCatalogo = new JLabel("Precio Alquiler Catalogo");
			lblPrecioAlquilerCatalogo.setBounds(10, 217, 117, 14);
			buttonPane.add(lblPrecioAlquilerCatalogo);

			JLabel lblTrastero = new JLabel("Metros Terreno");
			lblTrastero.setBounds(10, 250, 81, 14);
			buttonPane.add(lblTrastero);

			JLabel lblGranero = new JLabel("Granero");
			lblGranero.setBounds(10, 283, 56, 14);
			buttonPane.add(lblGranero);

			JLabel lblIdvivienda = new JLabel("Id_Vivienda");
			lblIdvivienda.setBounds(10, 19, 81, 14);
			buttonPane.add(lblIdvivienda);

			textField_IdViviendaRural = new JTextField();
			textField_IdViviendaRural.setToolTipText("");
			textField_IdViviendaRural.setBounds(137, 16, 86, 20);
			buttonPane.add(textField_IdViviendaRural);
			textField_IdViviendaRural.setColumns(10);

			textField_PrecioRural = new JTextField();
			textField_PrecioRural.setBounds(137, 82, 86, 20);
			buttonPane.add(textField_PrecioRural);
			textField_PrecioRural.setColumns(10);

			textField_DisponibilidadRural = new JTextField();
			textField_DisponibilidadRural.setColumns(10);
			textField_DisponibilidadRural.setBounds(138, 181, 86, 20);
			buttonPane.add(textField_DisponibilidadRural);

			textField_HabitacionesRural = new JTextField();
			textField_HabitacionesRural.setColumns(10);
			textField_HabitacionesRural.setBounds(138, 148, 86, 20);
			buttonPane.add(textField_HabitacionesRural);

			textField_MetrosRural = new JTextField();
			textField_MetrosRural.setColumns(10);
			textField_MetrosRural.setBounds(138, 115, 86, 20);
			buttonPane.add(textField_MetrosRural);

			textField_MetrosTerrenoRural = new JTextField();
			textField_MetrosTerrenoRural.setColumns(10);
			textField_MetrosTerrenoRural.setBounds(137, 244, 86, 20);
			buttonPane.add(textField_MetrosTerrenoRural);

			textField_PrecioAlquilerRural = new JTextField();
			textField_PrecioAlquilerRural.setColumns(10);
			textField_PrecioAlquilerRural.setBounds(137, 211, 86, 20);
			buttonPane.add(textField_PrecioAlquilerRural);
			
			rdbtnAlquiler = new JRadioButton("Alquiler");
			rdbtnAlquiler.setBounds(196, 48, 65, 23);
			buttonPane.add(rdbtnAlquiler);
			
			rdbtnVenta = new JRadioButton("Venta");
			rdbtnVenta.setBounds(129, 48, 65, 23);
			buttonPane.add(rdbtnVenta);
			
			rdbtnGraneroSi = new JRadioButton("Si");
			rdbtnGraneroSi.setBounds(137, 282, 41, 23);
			buttonPane.add(rdbtnGraneroSi);
			
			rdbtnGraneroNo = new JRadioButton("No");
			rdbtnGraneroNo.setBounds(196, 282, 52, 23);
			buttonPane.add(rdbtnGraneroNo);
			
			lblNewLabel_2 = new JLabel("");
			lblNewLabel_2.setIcon(new ImageIcon(VentanaInsertaRural.class.getResource("/img/chale_a_color_med.jpg")));
			lblNewLabel_2.setBounds(285, 74, 168, 144);
			buttonPane.add(lblNewLabel_2);
		}
	}
}
