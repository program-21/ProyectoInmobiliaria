package LP;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import LN.clsGestorLN;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

/**
 * Ventana que se encarga de gestionar la visualización de todos los clientes en
 * tablas o busqueda de uno de ellos por ID
 * 
 *
 * @author Asier y Natxo
 *
 */
public class VentanaMostrarClientes extends JFrame {

	private JPanel contentPane;
	private JTextField textidCliente;
	private JTextArea textArea;
	private JButton btnMostrartodos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaMostrarClientes frame = new VentanaMostrarClientes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaMostrarClientes() {
		setTitle("Buscar por DNI los Clientes Registrados en la App");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnBuscarClientes = new JButton("Buscar");
		btnBuscarClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textArea.setText(" ");

				String dni = textidCliente.getText();

				itfProperty x = clsGestorLN.obtenerGestorLN().recuperarCliente(dni);

				textArea.setText(" ");
				String cabecera = "DNI \tNombre \temail \tPassword \tidCliente\n";
				textArea.append(cabecera);
				textArea.append("=========================================================================\n");

				textArea.append((String) "" + x.getObjectProperty(clsConstantes.DNI));
				textArea.append((String) "\t\t" + x.getObjectProperty(clsConstantes.NOMBRE));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.EMAIL));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.PASSWORD));
				textArea.append((String) "\t" + x.getObjectProperty(clsConstantes.ID_CLIENTE));

				textArea.append("\n");

				textidCliente.setText("");
			}
		});

		textArea = new JTextArea();

		textidCliente = new JTextField();
		textidCliente.setColumns(10);

		btnMostrartodos = new JButton("MostrarTodos");
		btnMostrartodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Iterator<itfProperty> it = clsGestorLN.obtenerGestorLN().MostrarClientes();

				textArea.setText(" ");
				String cabecera = "DNI \tNombre \temail \tPassword \tidCliente\n";
				textArea.append(cabecera);
				textArea.append("=========================================================================\n");

				while (it.hasNext()) {
					itfProperty type = (itfProperty) it.next();

					String dni = (String) type.getObjectProperty(clsConstantes.DNI);
					String nombre = (String) type.getObjectProperty(clsConstantes.NOMBRE);
					String email = (String) type.getObjectProperty(clsConstantes.EMAIL);
					String password = (String) type.getObjectProperty(clsConstantes.PASSWORD);
					String idCliente = (String) type.getObjectProperty(clsConstantes.ID_CLIENTE);

					textArea.append("" + dni);
					textArea.append("\t\t" + nombre);
					textArea.append("\t" + email);
					textArea.append("\t" + password);
					textArea.append("\t" + idCliente);
					textArea.append("\n");
					textArea.append(
							"-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
				}
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup().addContainerGap().addComponent(
										textArea, GroupLayout.PREFERRED_SIZE, 407, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup().addGap(21)
										.addComponent(textidCliente, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(27).addComponent(btnBuscarClientes).addGap(34)
										.addComponent(btnMostrartodos)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup().addContainerGap()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(textidCliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnBuscarClientes).addComponent(btnMostrartodos))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(textArea, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)));
		contentPane.setLayout(gl_contentPane);
	}
}
