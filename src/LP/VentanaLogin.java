package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/**
 * Ventana que se encarga de gestionar la  el login de la App
 * Tanto para usuarios cliente, como para los que disponen de permisos de administrador
 * 
 *
 * @author Asier y Natxo
 *
 */
public class VentanaLogin extends JDialog {

	// definidos como atributos!!!

	private final JPanel contentPanel = new JPanel();

	private JTextField txtNombre;
	private JPasswordField txtPassword;

	// private clsGestorLN gln = new clsGestorLN();

	/**
	 * Create the dialog. Ventana de logeo para usuarios y administrador,
	 * dependiendo si eres admin o usuario-cliente tendr�s acceso a unas
	 * funcionalidades u otras.
	 */
	public VentanaLogin() {

		setModal(true);// le a�adimos esto para que no nos deje clicar en la ventana que se quede
						// flotando por debajo de esta

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {

				int r = JOptionPane.showConfirmDialog(null, "�Estas segur@?", "Salir", JOptionPane.YES_NO_OPTION);

				if (r == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

		setTitle("Acceso a la aplicaci\u00F3n");
		setResizable(false);

		// las dimensiones de la ventana
		setBounds(100, 100, 275, 230);

		setLocationRelativeTo(null); // centrado en la resolucion de tu monitor

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JLabel etiqNombre = new JLabel("Nombre");

		JLabel etiqPassword = new JLabel("Password");

		txtNombre = new JTextField();
		txtNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		txtNombre.setColumns(10);

		txtPassword = new JPasswordField();
		txtPassword.setHorizontalAlignment(SwingConstants.RIGHT);

		JButton btnRegistro = new JButton("Registro");
		btnRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				VentanaRegistro vr = new VentanaRegistro();
				vr.setVisible(true);

			}
		});
		btnRegistro.setHorizontalAlignment(SwingConstants.LEFT);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup().addContainerGap(27, Short.MAX_VALUE)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_contentPanel.createSequentialGroup().addComponent(etiqPassword)
										.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(txtPassword,
												GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup().addComponent(etiqNombre)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
						.addGap(38))
				.addGroup(Alignment.LEADING, gl_contentPanel.createSequentialGroup().addComponent(btnRegistro)
						.addContainerGap(186, Short.MAX_VALUE)));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPanel
				.createSequentialGroup().addGap(33)
				.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE).addComponent(etiqNombre).addComponent(
						txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(26)
				.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE).addComponent(etiqPassword)
						.addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED, 36, Short.MAX_VALUE).addComponent(btnRegistro)));
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						// si has escrito algo
						if (txtNombre.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Necesito usuario!");
						} else {

							char[] pass = txtPassword.getPassword();

							if (pass.length == 0) {
								JOptionPane.showMessageDialog(null, "Necesito contrase�a!");
							} else {
								// el usuario que has tecleado
								String strUser = txtNombre.getText();

								// el password que has tecleado
								String strPass = new String(pass);

								boolean r = clsGestorLN.obtenerGestorLN().comprobarUsuario(strUser, strPass);

								if (r == true) {
									// ocultar la ventana de login que ya no necesitas
									setVisible(false);

									// crear la segunda ventana

									VentanaHome sv = new VentanaHome(strUser);

									// muestra la nueva ventana

									sv.setVisible(true);
								} else {

									JOptionPane.showMessageDialog(null, "user/pass incorrecto!", "ERROR",
											JOptionPane.WARNING_MESSAGE, null);

									// JOptionPane.showMessageDialog(null, "USER/PASS incorrecto!");
								}
							}
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						int r = JOptionPane.showConfirmDialog(null, "�Estas segur@?", "Salir",
								JOptionPane.YES_NO_OPTION);

						if (r == JOptionPane.YES_OPTION) {
							System.exit(0);
						}
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
