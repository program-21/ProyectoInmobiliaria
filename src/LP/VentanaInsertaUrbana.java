package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
/**
 * Ventana que se encarga de gestionar la  inserción de viviendas urbanas en LN
 * para su posterior bolcado a base de datos
 * 
 * 
 * @author Asier y Natxo
 *
 */
public class VentanaInsertaUrbana extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField_IdViviendaUrbana;
	private JTextField textField_PrecioUrbana;
	private JTextField textField_DisponibilidadUrbana;
	private JTextField textField_HabitacionesUrbana;
	private JTextField textField_MetrosUrbana;
	private JTextField textField_PrecioAlquilerUrbana;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnVenta;
	private JRadioButton rdbtnAlquiler;
	private JRadioButton rdbtnSiTrastero;
	private JRadioButton rdbtnNoTrastero;
	private JRadioButton radioNoGaraje;
	private JRadioButton radioSiGaraje;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VentanaInsertaUrbana dialog = new VentanaInsertaUrbana();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaInsertaUrbana() {
		setTitle("INSERTAR VIVIENDA URBANA");
		setBounds(100, 100, 502, 355);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane);
			buttonPane.setLayout(null);
			contentPanel.setBounds(303, 5, 56, 24);
			buttonPane.add(contentPanel);
			contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPanel.setLayout(new BorderLayout(0, 0));
			{
				JButton GuardarButton = new JButton("Guardar");
				GuardarButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						String idVivienda = textField_IdViviendaUrbana.getText();
						String estado = "";

						if (rdbtnVenta.isSelected()) {
							estado = "venta";
						} else if (rdbtnAlquiler.isSelected()) {
							estado = "alquiler";
						}

						int precio = Integer.parseInt(textField_PrecioUrbana.getText());
						int metros = Integer.parseInt(textField_MetrosUrbana.getText());
						int habitaciones = Integer.parseInt(textField_HabitacionesUrbana.getText());
						String disponibilidad = textField_DisponibilidadUrbana.getText();
						int precioAlquiler = Integer.parseInt(textField_PrecioAlquilerUrbana.getText());

						Boolean trastero = null;

						if (rdbtnSiTrastero.isSelected()) {
							trastero = true;
						} else if (rdbtnNoTrastero.isSelected()) {
							trastero = false;
						}

						Boolean garaje = null;

						if (radioSiGaraje.isSelected()) {
							garaje = true;
						} else if (radioNoGaraje.isSelected()) {
							garaje = false;
						}

						boolean u = clsGestorLN.obtenerGestorLN().insertarUrbana(idVivienda, estado, precio, metros,
								habitaciones, disponibilidad, precioAlquiler, trastero, garaje);

						if (u == false) {

							JOptionPane.showMessageDialog(null, "Esta vivienda ya esta registrada");
						}

						/**
						 * Para vaciar los campos de la vivienda tras introducirla
						 */

						textField_IdViviendaUrbana.setText("");
						textField_PrecioUrbana.setText("");
						textField_MetrosUrbana.setText("");
						textField_HabitacionesUrbana.setText("");
						textField_DisponibilidadUrbana.setText("");
						textField_PrecioAlquilerUrbana.setText("");

					}
				});
				GuardarButton.setBounds(395, 282, 81, 23);
				GuardarButton.setActionCommand("OK");
				buttonPane.add(GuardarButton);
				getRootPane().setDefaultButton(GuardarButton);
			}
			{
				JLabel lblNewLabel = new JLabel("Estado");
				lblNewLabel.setBounds(10, 52, 81, 14);
				buttonPane.add(lblNewLabel);
			}
			{
				JLabel lblNewLabel_1 = new JLabel("Precio");
				lblNewLabel_1.setBounds(10, 85, 81, 14);
				buttonPane.add(lblNewLabel_1);
			}

			JLabel lblMetros = new JLabel("Metros");
			lblMetros.setBounds(10, 118, 81, 14);
			buttonPane.add(lblMetros);

			JLabel lblHabitaciones = new JLabel("Habitaciones");
			lblHabitaciones.setBounds(10, 151, 81, 14);
			buttonPane.add(lblHabitaciones);

			JLabel lblDisponibilidad = new JLabel("Disponibilidad");
			lblDisponibilidad.setBounds(10, 184, 81, 14);
			buttonPane.add(lblDisponibilidad);

			JLabel lblPrecioAlquilerCatalogo = new JLabel("Precio Alquiler Catalogo");
			lblPrecioAlquilerCatalogo.setBounds(10, 217, 127, 14);
			buttonPane.add(lblPrecioAlquilerCatalogo);

			JLabel lblTrastero = new JLabel("Trastero");
			lblTrastero.setBounds(10, 250, 81, 14);
			buttonPane.add(lblTrastero);

			JLabel lblGaraje = new JLabel("Garaje");
			lblGaraje.setBounds(10, 283, 81, 14);
			buttonPane.add(lblGaraje);

			JLabel lblIdvivienda = new JLabel("Id_Vivienda");
			lblIdvivienda.setBounds(10, 19, 81, 14);
			buttonPane.add(lblIdvivienda);

			textField_IdViviendaUrbana = new JTextField();
			textField_IdViviendaUrbana.setToolTipText("");
			textField_IdViviendaUrbana.setBounds(144, 16, 86, 20);
			buttonPane.add(textField_IdViviendaUrbana);
			textField_IdViviendaUrbana.setColumns(10);

			textField_PrecioUrbana = new JTextField();
			textField_PrecioUrbana.setBounds(144, 82, 86, 20);
			buttonPane.add(textField_PrecioUrbana);
			textField_PrecioUrbana.setColumns(10);

			textField_DisponibilidadUrbana = new JTextField();
			textField_DisponibilidadUrbana.setColumns(10);
			textField_DisponibilidadUrbana.setBounds(145, 181, 86, 20);
			buttonPane.add(textField_DisponibilidadUrbana);

			textField_HabitacionesUrbana = new JTextField();
			textField_HabitacionesUrbana.setColumns(10);
			textField_HabitacionesUrbana.setBounds(145, 148, 86, 20);
			buttonPane.add(textField_HabitacionesUrbana);

			textField_MetrosUrbana = new JTextField();
			textField_MetrosUrbana.setColumns(10);
			textField_MetrosUrbana.setBounds(145, 115, 86, 20);
			buttonPane.add(textField_MetrosUrbana);

			textField_PrecioAlquilerUrbana = new JTextField();
			textField_PrecioAlquilerUrbana.setColumns(10);
			textField_PrecioAlquilerUrbana.setBounds(144, 211, 86, 20);
			buttonPane.add(textField_PrecioAlquilerUrbana);

			rdbtnVenta = new JRadioButton("Venta");
			buttonGroup.add(rdbtnVenta);
			rdbtnVenta.setBounds(143, 48, 65, 23);
			buttonPane.add(rdbtnVenta);

			rdbtnAlquiler = new JRadioButton("Alquiler");
			buttonGroup.add(rdbtnAlquiler);
			rdbtnAlquiler.setBounds(210, 48, 109, 23);
			buttonPane.add(rdbtnAlquiler);

			rdbtnSiTrastero = new JRadioButton("Si");
			rdbtnSiTrastero.setBounds(144, 246, 38, 23);
			buttonPane.add(rdbtnSiTrastero);

			rdbtnNoTrastero = new JRadioButton("No");
			rdbtnNoTrastero.setBounds(210, 246, 109, 23);
			buttonPane.add(rdbtnNoTrastero);

			radioNoGaraje = new JRadioButton("No");
			radioNoGaraje.setBounds(210, 282, 109, 23);
			buttonPane.add(radioNoGaraje);

			radioSiGaraje = new JRadioButton("Si");
			radioSiGaraje.setBounds(144, 282, 38, 23);
			buttonPane.add(radioSiGaraje);
			
			lblNewLabel_2 = new JLabel("New label");
			lblNewLabel_2.setIcon(new ImageIcon(VentanaInsertaUrbana.class.getResource("/img/pis_colo_tras_dime.png")));
			lblNewLabel_2.setBounds(303, 85, 151, 133);
			buttonPane.add(lblNewLabel_2);
		}
	}
}
