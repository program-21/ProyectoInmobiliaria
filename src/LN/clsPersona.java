package LN;

import COMUN.itfProperty;
import COMUN.clsConstantes;

import COMUN.DNIIncorrectoException;

/**
 * 
 * Clase padre abstarcta de la que heredan parte de los atributos las entidades
 * Cliente y Vendedor
 * 
 * @author Asier y Natxo
 *
 */
public abstract class clsPersona implements itfProperty, Comparable<clsPersona> {

	private String dni;

	private String nombre;

	private String email;

	private String password;

	/**
	 * @param dni
	 * @param nombre
	 * @param email
	 */
	public clsPersona(String dni, String nombre, String email, String password) throws DNIIncorrectoException { // <---
																												// exigido
																												// si
		// provocas la excepcion dentro

		char letra = dni.charAt(8);

		if (letra >= 'a' && letra <= 'z') {
			throw new DNIIncorrectoException("La letra debe ser mayuscula!");
		} else if (letra < 'A' || letra > 'Z') {
			throw new DNIIncorrectoException("Falta la letra!");
		}

		// everything is oK!

		this.dni = dni;
		this.nombre = nombre;
		this.email = email;
		this.password = password;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof clsPersona))
			return false;
		clsPersona other = (clsPersona) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		return true;
	}

	@Override
	public Object getObjectProperty(String propiedad) {
		switch (propiedad) {
		case clsConstantes.DNI:
			return dni;
		case clsConstantes.NOMBRE:
			return nombre;
		case clsConstantes.EMAIL:
			return email;
		case clsConstantes.PASSWORD:
			return password;

		default:
			// return null;
			throw new PropiedadNoDisponibleException(); // <--- IMPLICITA!!!
		}
	}

	@Override
	public String toString() {
		return "clsPersona [dni=" + dni + ", nombre=" + nombre + ", email=" + email + ", password=" + password + "]";
	}

	@Override
	public int compareTo(clsPersona o) {
		// dos objetos de la misma clase se comparan por un unico criterio!

		return dni.compareToIgnoreCase(o.dni);

	}

}
