package LN;

/**
 * Esta clase recoge identificador autonum�rico de la transaccion y un atributo
 * descripci�n en el que se detallar� si es alquiler o venta
 * 
 * Est� clase se quedar� sin implementar y sin funcionalidad asociada debido a
 * la falta de tiempo para realizar toda la funcionalidad sobredimensionada en
 * un inicio
 * 
 * @author Asier y Natxo
 *
 */
public class clsTipoTransaccion {

	private String idTipoTrans;
	private String descripcion;

	public clsTipoTransaccion() {
		idTipoTrans = "";
		descripcion = "";
	}

	/**
	 * @return the idTipoTrans
	 */
	public String getIdTipoTrans() {
		return idTipoTrans;
	}

	/**
	 * @param idTipoTrans the idTipoTrans to set
	 */
	public void setIdTipoTrans(String idTipoTrans) {
		this.idTipoTrans = idTipoTrans;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @param idTipoTrans
	 * @param descripcion
	 */
	public clsTipoTransaccion(String idTipoTrans, String descripcion) {
		super();
		this.idTipoTrans = idTipoTrans;
		this.descripcion = descripcion;
	}

}
