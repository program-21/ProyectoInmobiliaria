package LN;

import COMUN.DNIIncorrectoException;
import COMUN.clsConstantes;
import COMUN.itfProperty;

/**
 * Esta es la clase que define un cliente, la cual hereda varios atributos de la
 * clase padre persona.
 * 
 * @author Asier y Natxo
 *
 */
public class clsCliente extends clsPersona {

	private String idCliente;

	/**
	 * @param dni
	 * @param nombre
	 * @param email
	 * @param password
	 * @param idCliente
	 */
	public clsCliente(String dni, String nombre, String email, String password, String idCliente)
			throws DNIIncorrectoException {
		super(dni, nombre, email, password);

		this.idCliente = idCliente;
	}

	@Override
	public Object getObjectProperty(String propiedad) {
		switch (propiedad) {
		case clsConstantes.ID_CLIENTE:
			return idCliente;

		default:
			return super.getObjectProperty(propiedad);
		}
	}

}
