package LN;

import COMUN.clsConstantes;
import COMUN.itfProperty;

/**
 * Esta clase es abstracta para que por higiene de programación no nos deje
 * crear objetos de la misma. Clase padre de vivienda urbana y vivienda rural
 * para gestionar la información correspondiente
 * 
 * @author Industria 4.0 Asier y Natxo
 *
 */
public abstract class clsVivienda implements itfProperty, Comparable<clsVivienda> {

	private String idVivienda;

	private String estado;

	private int precio;

	private int metros;

	private int habitaciones;

	private String disponibilidad;

	private int precioAlquilerCatalogo;

	public clsVivienda() {

		idVivienda = "";
		estado = "";
		precio = 0;
		metros = 0;
		habitaciones = 0;
		disponibilidad = "";
		precioAlquilerCatalogo = 0;

	}

	public String getIdVivienda() {
		return idVivienda;
	}

	public void setIdVivienda(String idVivienda) {
		this.idVivienda = idVivienda;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getMetros() {
		return metros;
	}

	public void setMetros(int metros) {
		this.metros = metros;
	}

	public int getHabitaciones() {
		return habitaciones;
	}

	public void setHabitaciones(int habitaciones) {
		this.habitaciones = habitaciones;
	}

	public String getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	public int getPrecioAlquilerCatalogo() {
		return precioAlquilerCatalogo;
	}

	public void setPrecioAlquilerCatalogo(int precioAlquilerCatalogo) {
		this.precioAlquilerCatalogo = precioAlquilerCatalogo;
	}

	public clsVivienda(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo) {
		super();
		this.idVivienda = idVivienda;
		this.estado = estado;
		this.precio = precio;
		this.metros = metros;
		this.habitaciones = habitaciones;
		this.disponibilidad = disponibilidad;
		this.precioAlquilerCatalogo = precioAlquilerCatalogo;
	}

	/**
	 * Este metodo me comparará las viviendas por el ID cuando no le de un
	 * comparador específico
	 */
	@Override
	public int compareTo(clsVivienda o) {
		return idVivienda.compareTo(o.idVivienda);
	}

	@Override
	public Object getObjectProperty(String propiedad) {
		switch (propiedad) {

		case clsConstantes.ID_VIVIENDA:
			return idVivienda;
		case clsConstantes.ESTADO:
			return estado;
		case clsConstantes.PRECIO:
			return precio;
		case clsConstantes.METROS:
			return metros;
		case clsConstantes.HABITACIONES:
			return habitaciones;
		case clsConstantes.DISPONIBILIDAD:
			return disponibilidad;
		case clsConstantes.PRECIO_ALQUILER_CATALOGO:
			return precioAlquilerCatalogo;

		default:
			return null;
		}
	}

	@Override
	public String toString() {
		return "clsVivienda [idVivienda=" + idVivienda + ", estado=" + estado + ", precio=" + precio + ", metros="
				+ metros + ", habitaciones=" + habitaciones + ", disponibilidad=" + disponibilidad
				+ ", precioAlquilerCatalogo=" + precioAlquilerCatalogo + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idVivienda == null) ? 0 : idVivienda.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		clsVivienda other = (clsVivienda) obj;
		if (idVivienda == null) {
			if (other.idVivienda != null)
				return false;
		} else if (!idVivienda.equals(other.idVivienda))
			return false;
		return true;
	}

}
