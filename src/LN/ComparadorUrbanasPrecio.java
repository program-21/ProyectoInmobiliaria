package LN;

import java.util.Comparator;

import COMUN.itfProperty;
import COMUN.clsConstantes;

/**
 * Clase encargada de implementar el metodo comparator para poder ordenar las
 * viviendas urbanas por precio ascendente o descendente en funci�n del valor
 * booleano que se le pasa al m�todo que implementa esta clase
 * 
 * @author Industria 4.0
 *
 */
public class ComparadorUrbanasPrecio implements Comparator<itfProperty> {

	private boolean ascendente;

	public ComparadorUrbanasPrecio(boolean ascendente) {

		this.ascendente = ascendente;

	}

	@Override
	public int compare(itfProperty o1, itfProperty o2) {

		// o1 <--- este es el primer objeto en la comparacion
		// o2 <--- este es el segundo objeto en la comparacion

		int precio1 = 0;
		int precio2 = 0;

		precio1 = (int) o1.getObjectProperty(clsConstantes.PRECIO);
		precio2 = (int) o2.getObjectProperty(clsConstantes.PRECIO);

		if (ascendente) {
			return precio1 - precio2;

		} else {
			return precio2 - precio1;
		}

	}

}
