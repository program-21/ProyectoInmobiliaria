package LN;

import COMUN.DNIIncorrectoException;
import COMUN.clsConstantes;

/**
 * Esta es la clase para crear la entidad vendedor, la cual hereda de la clase
 * abstarcta persona
 * 
 * @author Asier y Natxo
 *
 */
public class clsVendedor extends clsPersona {

	private String idVendedor;

	/**
	 * @param dni
	 * @param nombre
	 * @param email
	 * @param password
	 * @param idVendedor
	 */
	public clsVendedor(String dni, String nombre, String email, String password, String idVendedor)
			throws DNIIncorrectoException {
		super(dni, nombre, email, password);
		this.idVendedor = idVendedor;
	}

	@Override
	public Object getObjectProperty(String propiedad) {
		switch (propiedad) {
		case clsConstantes.ID_VENDEDOR:
			return idVendedor;

		default:
			return super.getObjectProperty(propiedad);
		}
	}
}
