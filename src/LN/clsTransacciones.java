package LN;

/**
 * Clase que registra el c�digo y la fecha de la transaccion con las viviendas
 * 
 * Est� clase se quedar� sin implementar y sin funcionalidad asociada debido a
 * la falta de tiempo para realizar toda la funcionalidad sobredimensionada en
 * un inicio
 * 
 * @author Asier y Natxo
 *
 */

public class clsTransacciones {

	private int idTrans;
	private String fechaTrans;

	public clsTransacciones() {
		idTrans = 0;
		fechaTrans = "";
	}

	public int getIdTrans() {
		return idTrans;
	}

	public void setIdTrans(int idTrans) {
		this.idTrans = idTrans;
	}

	public String getFechaTrans() {
		return fechaTrans;
	}

	public void setFechaTrans(String fechaTrans) {
		this.fechaTrans = fechaTrans;
	}

	/**
	 * @param idTrans
	 * @param fechaTrans
	 */
	public clsTransacciones(int idTrans, String fechaTrans) {
		super();
		this.idTrans = idTrans;
		this.fechaTrans = fechaTrans;
	}

}
