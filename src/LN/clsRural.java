package LN;

import COMUN.clsConstantes;

/**
 * Esta clase es la encargada de crear distintas vivendas rurales. Esta clase
 * hereda los atributos y m�todos de su clase padre que es clsVivienda. En
 * a�adidura a estos atributos heredados, adem�s posee otros propios que son
 * metrosTerreno y granero.
 * 
 * @author Asier y Natxo
 *
 */
public class clsRural extends clsVivienda {

	@Override
	public String toString() {
		return super.toString() + "clsRural [metrosTerreno=" + metrosTerreno + ", granero=" + granero + "]";
	}

	private int metrosTerreno;
	private boolean granero;

	public clsRural(String idVivienda, String estado, int precio, int metros, int habitaciones, String disponibilidad,
			int precioAlquilerCatalogo, int metrosTerreno, boolean granero) {

		super(idVivienda, estado, precio, metros, habitaciones, disponibilidad, precioAlquilerCatalogo);

		this.granero = granero;
		this.metrosTerreno = metrosTerreno;
	}

	public int getMetrosTerreno() {
		return metrosTerreno;
	}

	public void setMetrosTerreno(int metrosTerreno) {
		this.metrosTerreno = metrosTerreno;
	}

	public boolean getGranero() {
		return granero;
	}

	public void setGranero(boolean granero) {
		this.granero = granero;
	}

	@Override
	public Object getObjectProperty(String propiedad) {
		switch (propiedad) {
		case clsConstantes.GRANERO:
			return granero;
		case clsConstantes.METROS_TERRENO:
			return metrosTerreno;

		default:
			return super.getObjectProperty(propiedad);
		}
	}

}
