package LN;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import COMUN.itfProperty;
import COMUN.DNIIncorrectoException;
import COMUN.IdIncorrectoExcepcion;
import COMUN.clsConstantes;
import LD.BDException;
import LD.clsFila;
import LD.clsGestorLD;
import LP.clsUtilidadesLP;
/**
 * 
 * Clase encargada de gestionar todos los datos de la aplicaci�n, que implementa
 * el patr�n singleton
 * 
 * @author Asier y Natxo
 *
 */
public class clsGestorLN {

	// IMPLEMENTAR EL PATRON SINGLETON

	/*
	 * 1. poner el constructor de la clase privado - para que no se puedan crear
	 * objetos cuando te de la gana - para que no puedas crear MUCHOS objetos de
	 * este tipo
	 * 
	 * 2. poner una variable en la clase que sea estatica - para poder tener acceso
	 * al objeto, pero esta variable es privada - no puedes permitir que cualquiera
	 * la modifique/cambie/acceda - acceso restringido
	 * 
	 * 3. poner un metodo en la clase que sea estatico. - para poder solicitar el
	 * gestor desde cualquier parte del programa
	 * 
	 */

	// *********** PATRON SINGLETON *******************

	// patron de objeto �nico en el sistema

	// 1. el constructor debe ser privado
	private clsGestorLN() {
		// ==============================================ESTRUCURAS--GUARDAR--DATOS===================================
		// todos los datos/tablas que necesita el gestor LN
		misViviendasRurales = new HashSet<>();
		misViviendasUrbanas = new HashSet<>();
		vendedores = new HashSet<>();
		clientes = new HashSet<>();

	}
	// ===============================================================================================
	// 2. una variable que siempre referencie a este objeto, y debe ser privada

	private static clsGestorLN referenciaAlObjetoUnico = new clsGestorLN();
	// ================================================================================================0
	// 3.metodo de acceso al objeto Singleton

	public static clsGestorLN obtenerGestorLN() {
		return referenciaAlObjetoUnico;
	}

	// ==========================================================================================================0
	private Set<itfProperty> misViviendasRurales;
	private Set<itfProperty> misViviendasUrbanas;
	private Set<itfProperty> vendedores;
	private Set<itfProperty> clientes;

	private clsGestorLD gld = new clsGestorLD();

//========================================CREA--VIVIENDA--URBANA--Y--LA--GUARDA--EN--LN======================================
	/**
	 * Este m�todo se encarga de recoger los atributos de vivienda urbana que
	 * inserta el usuario desde la l�gica de presentaci�n, para anadirlos a nuestro
	 * Hashset misViviendasUrbanas.
	 * 
	 * @param idVivienda
	 * @param estado
	 * @param precio
	 * @param metros
	 * @param habitaciones
	 * @param disponibilidad
	 * @param precioAlquilerCatalogo
	 * @param trastero
	 * @param garaje
	 */
	public boolean insertarUrbana(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo, boolean trastero, boolean garaje)
			throws IdIncorrectoExcepcion {

		clsUrbana u = new clsUrbana(idVivienda, estado, precio, metros, habitaciones, disponibilidad,
				precioAlquilerCatalogo, trastero, garaje);
		if (misViviendasUrbanas.contains(u) == false) {

			misViviendasUrbanas.add(u);
			return true;
		} else {
			return false;
		}

	}

//========================================CREA--VIVIENDA--RURAL--Y--LA--GUARDA--EN--LN======================================
	/**
	 * Este m�todo se encarga de recoger los atributos de vivienda rural que inserta
	 * el usuario desde la l�gica de presentaci�n, para anadirlos a nuestro Hashset
	 * misViviendasRurales.
	 * 
	 * @param idVivienda
	 * @param estado
	 * @param precio
	 * @param metros
	 * @param habitaciones
	 * @param disponibilidad
	 * @param precioAlquilerCatalogo
	 * @param metrosTerreno
	 * @param granero
	 */
	public boolean insertarRural(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo, int metrosTerreno, boolean granero)
			throws IdIncorrectoExcepcion {

		clsRural r = new clsRural(idVivienda, estado, precio, metros, habitaciones, disponibilidad,
				precioAlquilerCatalogo, metrosTerreno, granero);

		if (misViviendasRurales.contains(r) == false) {

			misViviendasRurales.add(r);
			return true;
		} else {
			return false;
		}

	}

// ============================================RECUPERAR--URBANA--POR--ID=================================================================
	/**
	 * Este metodo utiliza el m�todo contains que implementan las estrucuturas Hash,
	 * buscando por el ID la vivienda urbana que buscamos y que se encuentre en las
	 * estructuras de la LN , y nos la devuelve.
	 * 
	 * @param idVivienda
	 * @return v
	 */
	public itfProperty recuperarUrbana(String idVivienda) {

		System.out
				.println(misViviendasUrbanas.contains(new clsUrbana(idVivienda, null, 0, 0, 0, null, 0, false, false)));

		try {
			for (itfProperty v : misViviendasUrbanas) {
				String idQueEstasMirando = ((String) v.getObjectProperty(clsConstantes.ID_VIVIENDA));

				if (idQueEstasMirando.equals(idVivienda)) {
					System.out.println("Encontrada Urbana!!!");
					return v;
				}
			}

		} catch (IdIncorrectoExcepcion e) {

			return null; // <---- este null es por si el ID NO es VALIDO!!!!!

		}

		return null;
	}

//==========================================RECUPERAR--RURAL--POR--ID==============================================================
	/**
	 * Este metodo utiliza el m�todo contains que implementan las estrucuturas Hash,
	 * buscando por el ID la vivienda rural que buscamos en la LN, y nos la
	 * devuelve.
	 * 
	 * @param idVivienda
	 * @return c
	 */
	public itfProperty recuperarRural(String idVivienda) {

		System.out.println(misViviendasRurales.contains(new clsRural(idVivienda, null, 0, 0, 0, null, 0, 0, false)));

		try {
			for (itfProperty c : misViviendasRurales) {
				String idQueEstasMirando = ((String) c.getObjectProperty(clsConstantes.ID_VIVIENDA));

				if (idQueEstasMirando.equals(idVivienda)) {
					System.out.println("Encontrada Rural!!!");

					return c;
				}
			}
		} catch (IdIncorrectoExcepcion e) {

			return null; // <---- este null es por si el ID NO es VALIDO!!!!!

		}

		return null;
	}

// =====================================================BORRAR--URBANA--LN--Y--BD=========================================================
	/**
	 * Este m�todo se encarga de eliminar simult�neamente de la LN y de la BD, una
	 * vivienda urbana por el ID que recibe esta funci�n.
	 * 
	 * @param idVivienda
	 */
	public void borrarUrbana(String idVivienda) {

		itfProperty u = new clsUrbana(idVivienda, null, 0, 0, 0, null, 0, false, false);

		System.out.println("MOSTRAR LA TABLA DE VIVIENDAS");
		System.out.println(misViviendasUrbanas);

		// NO ENCUENTRA TU VIVIENDA POR LO QUE NO LA PUEDE BORRAR
		if (misViviendasUrbanas.contains(u))
			System.out.println("He encontrado la vivienda!!!");

		boolean b = misViviendasUrbanas.remove(u);

		System.out.println("Resultado del borrado");
		System.out.println(b);

		gld.borrarUrbana(idVivienda);

	}

//===================================================BORRAR--RURAL--LN--Y--BD======================================================
	/**
	 * Este m�todo se encarga de eliminar simult�neamente de la LN y de la BD, una
	 * vivienda rual por el ID que recibe esta funci�n.
	 * 
	 * @param idVivienda
	 */
	public void borrarRural(String idVivienda) {

		itfProperty r = new clsRural(idVivienda, null, 0, 0, 0, null, 0, 0, false);

		System.out.println("MOSTRAR LA TABLA DE VIVIENDAS");
		System.out.println(misViviendasRurales);

		// NO ENCUENTRA TU VIVIENDA POR LO QUE NO LA PUEDE BORRAR
		if (misViviendasRurales.contains(r))
			System.out.println("He encontrado la vivienda!!!");

		boolean b = misViviendasRurales.remove(r);

		System.out.println("Resultado del borrado");
		System.out.println(b);

		gld.borrarRural(idVivienda);

	}

// ===============================================GUARDAR--VIVIENDAS--URBANAS--EN--BD=====================================================
	/**
	 * almacena todos los datos del Hashset minViviendasUrbanas de LN en BD
	 */

	public void guardarUrbanaLD() {

		Iterator<itfProperty> it = misViviendasUrbanas.iterator();

		itfProperty u;

		while (it.hasNext()) {
			u = it.next();

			String idVivienda = (String) u.getObjectProperty(clsConstantes.ID_VIVIENDA);

			String estado = (String) u.getObjectProperty(clsConstantes.ESTADO);

			int precio = (int) u.getObjectProperty(clsConstantes.PRECIO);

			int metros = (int) u.getObjectProperty(clsConstantes.METROS);

			int habitaciones = (int) u.getObjectProperty(clsConstantes.HABITACIONES);

			String disponibilidad = (String) u.getObjectProperty(clsConstantes.DISPONIBILIDAD);

			int precioAlquilerCatalogo = (int) u.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);

			boolean trastero = (Boolean) u.getObjectProperty(clsConstantes.TRASTERO);

			boolean garaje = (Boolean) u.getObjectProperty(clsConstantes.GARAJE);

			gld.insertarUrbana(idVivienda, estado, precio, metros, habitaciones, disponibilidad, precioAlquilerCatalogo,
					trastero, garaje);

		}
	}

// ===============================================GUARDAR--VIVIENDAS--RURALES--EN--BD====================================================
	/**
	 * almacena todos los datos del Hashset minViviendasRurales de LN en BD
	 */
	public void guardarRuralLD() {

		Iterator<itfProperty> it = misViviendasRurales.iterator();

		itfProperty r;

		while (it.hasNext()) {
			r = it.next();

			String idVivienda = (String) r.getObjectProperty(clsConstantes.ID_VIVIENDA);

			String estado = (String) r.getObjectProperty(clsConstantes.ESTADO);

			int precio = (int) r.getObjectProperty(clsConstantes.PRECIO);

			int metros = (int) r.getObjectProperty(clsConstantes.METROS);

			int habitaciones = (int) r.getObjectProperty(clsConstantes.HABITACIONES);

			String disponibilidad = (String) r.getObjectProperty(clsConstantes.DISPONIBILIDAD);

			int precioAlquilerCatalogo = (int) r.getObjectProperty(clsConstantes.PRECIO_ALQUILER_CATALOGO);

			int metrosTerreno = (int) r.getObjectProperty(clsConstantes.METROS_TERRENO);
			boolean granero = (boolean) r.getObjectProperty(clsConstantes.GRANERO);

			gld.insertarRural(idVivienda, estado, precio, metros, habitaciones, disponibilidad, precioAlquilerCatalogo,
					metrosTerreno, granero);
		}

	}

// ========================================GUARDA--TODOS--LOS--DATOS--EN--BD====================================================
	/**
	 * Este m�todo se encargar� de volcar las estructuras con las viviendas que
	 * esten en las estructuras de viviendas llevandolas a la BD
	 */
	public void guardarLD() {
		guardarUrbanaLD();
		guardarRuralLD();
		guardarVendedorLD();
		guardarClienteLD();
	}

// ==============================================MOSTRAR VIVIENDAS URBANAS DE LA LN=============================================
	public Iterator<itfProperty> recuperarDatosUrbanas() {

		return misViviendasUrbanas.iterator();

	}

	/**
	 * M�todo que muestra las viviendas Urbanas de la LN
	 */

	public void mostrarDatosUrbanas() {

		Iterator<itfProperty> it = misViviendasUrbanas.iterator();

		itfProperty u;

		while (it.hasNext()) {
			u = it.next();
			System.out.println(u);
		}

	}

// =========================================MOSTRAR VIVIENDAS RURALES DE LA LN=====================================================

	public Iterator<itfProperty> recuperarDatosRurales() {

		return misViviendasRurales.iterator();

	}

	/**
	 * M�todo que muestra las viviendas rurales de la LN
	 */

	public void mostrarDatosRurales() {
		Iterator<itfProperty> ot = misViviendasRurales.iterator();

		itfProperty r;

		while (ot.hasNext()) {
			r = ot.next();
			System.out.println(r);
		}
		;
	}

	// ==================================================RURALES--ASCENDENTES--POR--PRECIO============================================================

	public List<itfProperty> mostrarRuralesPrecioAscendente() {

		Comparator<itfProperty> comparador = new ComparadorRuralesPrecio(true);// si mando false descendente

		List<itfProperty> misViviendasRuralesOrdenadas = clsUtilidadesLP.ordenar(misViviendasRurales, comparador); // <---
																													// diferente
																													// comparador,
																													// diferente
																													// orden

		for (itfProperty clsUrbana : misViviendasRuralesOrdenadas) {
			System.out.println(clsUrbana.getObjectProperty(clsConstantes.PRECIO));
		}

		return misViviendasRuralesOrdenadas;

	}

// ==================================================URBANAS--ASCENDENTES--POR--PRECIO============================================================
	/**
	 * Metodo encargado de mostrar las viviendas Urbanas ordenadas por precio
	 * ascendente
	 * 
	 * @return misViviendasUrbanasOrdenadas
	 */
	public List<itfProperty> mostrarUrbanasPrecioAscendente() {

		// Set<itfProperty> misViviendasUrbanas = new HashSet<itfProperty>();

		Comparator<itfProperty> comparador = new ComparadorUrbanasPrecio(true);// si mando false descendente

		List<itfProperty> misViviendasUrbanasOrdenadas = clsUtilidadesLP.ordenar(misViviendasUrbanas, comparador); // <---
																													// diferente
																													// comparador,
																													// diferente
																													// orden

		for (itfProperty clsUrbana : misViviendasUrbanasOrdenadas) {
			System.out.println(clsUrbana.getObjectProperty(clsConstantes.PRECIO));
		}

		return misViviendasUrbanasOrdenadas;

	}

// ================================================RECUPERAR--URBANAS--DE--BD===========================================================

	/**
	 * Este m�todo nos recupera todas las viviendas Urbanas de la base de Dato para
	 * insertarlas en las estructura de la LN para que las consuktas del usuario se
	 * hagan sobre la LN sin tener que ir cada vez a la BD.
	 */
	public void recuperarViviendasUrbanasBD() {

		// Conecto a la base de datos.
		gld.conectar();

		// Leo la informaci�n y la uso.

		Set<clsFila> filasQueRecuperas = gld.getUrbanas();

		String idViviendaRec;
		String estadoRec;
		int precioRec;
		int metrosRec;
		int habitacionesRec;
		String disponibilidadRec;
		int precioAlquilerCatalogoRec;
		boolean trasteroRec;
		boolean garajeRec;

		itfProperty u;

		for (clsFila f : filasQueRecuperas) {
			idViviendaRec = (String) f.getData("idVivienda");
			estadoRec = (String) f.getData("estado");
			precioRec = (int) f.getData("precio");
			metrosRec = (int) f.getData("metros");
			habitacionesRec = (int) f.getData("habitaciones");
			disponibilidadRec = (String) f.getData("disponibilidad");
			precioAlquilerCatalogoRec = (int) f.getData("precioAlquilerCatalogo");
			trasteroRec = (boolean) f.getData("trastero");
			garajeRec = (boolean) f.getData("garaje");

			u = new clsUrbana(idViviendaRec, estadoRec, precioRec, metrosRec, habitacionesRec, disponibilidadRec,
					precioAlquilerCatalogoRec, trasteroRec, garajeRec);

			misViviendasUrbanas.add(u);
		}

		// Libero los recursos.
		gld.desconectar();

	}

// =============================RECUPERAR--RURALES--DE--BD=================================
	/**
	 * Este m�todo nos recupera todas las viviendas Rurales de la base de Dato para
	 * insertarlas en las estructura de la LN para que las consuktas del usuario se
	 * hagan sobre la LN sin tener que ir cada vez a la BD.
	 * 
	 */
	public void recuperarViviendasRuralesBD() {

		// Conecto a la base de datos.
		gld.conectar();

		// Leo la informaci�n y la uso.

		Set<clsFila> filasQueRecuperas = gld.getRurales();

		String idViviendaRec;
		String estadoRec;
		int precioRec;
		int metrosRec;
		int habitacionesRec;
		String disponibilidadRec;
		int precioAlquilerCatalogoRec;
		int metrosTerrenoRec;
		boolean graneroRec;

		itfProperty r;

		for (clsFila f : filasQueRecuperas) {
			idViviendaRec = (String) f.getData("idVivienda");
			estadoRec = (String) f.getData("estado");
			precioRec = (int) f.getData("precio");
			metrosRec = (int) f.getData("metros");
			habitacionesRec = (int) f.getData("habitaciones");
			disponibilidadRec = (String) f.getData("disponibilidad");
			precioAlquilerCatalogoRec = (int) f.getData("precioAlquilerCatalogo");
			metrosTerrenoRec = (int) f.getData("metrosTerreno");
			graneroRec = (boolean) f.getData("granero");

			r = new clsRural(idViviendaRec, estadoRec, precioRec, metrosRec, habitacionesRec, disponibilidadRec,
					precioAlquilerCatalogoRec, metrosTerrenoRec, graneroRec);

			misViviendasRurales.add(r);

			// Libero los recursos.
			gld.desconectar();

		}
	}

//=====================================================VENDEDORES=======================================================
	/**
	 * 
	 * @param dni
	 * @param nombre
	 * @param email
	 * @param password
	 * @param idVendedor
	 * @return
	 * @throws DNIIncorrectoException
	 */
	// a�ade un vendedor al LN
	public boolean insertaVendedor(String dni, String nombre, String email, String password, String idVendedor)
			throws DNIIncorrectoException {

		// hay que gestionar el posible error al crear el vendedor debido al DNI

		// 1. propagar el error... throws DNIIncorrectoException en la cabecera del
		// metodo

		// 2. gestionar ese error para que no siga en el programa, ESTO NO!!! DEBE
		// GESTIONARSE EN LA LP

		boolean error = false;

		clsVendedor p = new clsVendedor(dni, nombre, email, password, idVendedor);

		error = vendedores.add(p);

		return error;

	}

	/**
	 * 
	 * @throws BDException
	 */
	public void guardarVendedorLD() {

		Iterator<itfProperty> it = vendedores.iterator();

		itfProperty u;

		while (it.hasNext()) {
			u = it.next();

			String dni = (String) u.getObjectProperty(clsConstantes.DNI);

			String nombre = (String) u.getObjectProperty(clsConstantes.NOMBRE);

			String email = (String) u.getObjectProperty(clsConstantes.EMAIL);

			String password = (String) u.getObjectProperty(clsConstantes.PASSWORD);

			String idVendedor = (String) u.getObjectProperty(clsConstantes.ID_VENDEDOR);

			gld.insertarVendedorBD(dni, nombre, email, password, idVendedor);

		}
	}

	/**
	 * 
	 * @param dni
	 */
	// de la LN
	public void borrarVendedor(String dni) {

		itfProperty v = new clsVendedor(dni, null, null, null, null);

		System.out.println("MOSTRAR LA TABLA DE VENDEDORES");
		System.out.println(vendedores);

		// NO ENCUENTRA TU VIVIENDA POR LO QUE NO LA PUEDE BORRAR
		if (vendedores.contains(v))
			System.out.println("He encontrado al vendedor!!!");

		boolean b = vendedores.remove(v);

		System.out.println("Resultado del borrado");
		System.out.println(b);

		gld.borrarVendedor(dni);
	}

	/**
	 * 
	 * @param dni
	 * @return
	 */
	// se recupera de la LN no de la LD
	public itfProperty recuperarVendedor(String dni) {
		return null;
	}

	public void recuperarVendedorLD(String dni) {
		clsFila x = gld.recuperarVendedor(dni);

		String elDniQueRecuperas = (String) x.getData("dni");
		String elNombreQueRecuperas = (String) x.getData("nombre");
		String elEmailQueRecuperas = (String) x.getData("email");
		String elPasswordQueRecuperas = (String) x.getData("password");
		String elIdVendedorQueRecuperas = (String) x.getData("idVendedor");

		clsVendedor a = new clsVendedor(elDniQueRecuperas, elNombreQueRecuperas, elEmailQueRecuperas,
				elPasswordQueRecuperas, elIdVendedorQueRecuperas);

		vendedores.add(a);
	}

	/**
	 * 
	 * @param dni
	 * @param nombre
	 * @param email
	 * @param password
	 * @param idVendedor
	 */
	public void modificarVendedor(String dni, String nombre, String email, String password, String idVendedor) {
	}

	/**
	 * 
	 */
	// se recuperan de la LD
	public void recuperarVendedores() {
		// ir la LD, recuperar la info de los alumnos mediante clsFila y los
		// voy a guardar en la LN como itfProperty, asi podras ordenarlos, etc,
		// etc,etc...
		// porque en la capa LN los datos deben ser itfProperty (NO CASTING) ( SI
		// WRAPING )

		// recuperas desde la LD
		Set<clsFila> filasQueRecuperas = gld.recuperarVendedores();

		// declarar las variables fuera del bucle
		String dni;
		String nombre;
		String email;
		String password;
		String idVendedor;

		itfProperty v;

		// por cada alumno recuperado de la LD
		for (clsFila f : filasQueRecuperas) {
			// recuperar cada dato de la clsFila de la LD
			dni = (String) f.getData("dni");
			nombre = (String) f.getData("nombre");
			email = (String) f.getData("email");
			password = (String) f.getData("password");
			idVendedor = (String) f.getData("idVendedor");

			// crear un itfProperty
			v = new clsVendedor(dni, nombre, email, password, idVendedor);

			// a�adir ese objeto al LN
			vendedores.add(v);
		}
	}

//=====================================================CLIENTES=======================================================
	/**
	 * 
	 * @param dni
	 * @param nombre
	 * @param email
	 * @param password
	 * @param idCliente
	 * @return
	 * @throws DNIIncorrectoException
	 */
	// a�ade un cliente al LN
	public boolean insertaCliente(String dni, String nombre, String email, String password, String idCliente)
			throws DNIIncorrectoException {

		// hay que gestionar el posible error al crear el vendedor debido al DNI

		// 1. propagar el error... throws DNIIncorrectoException en la cabecera del
		// metodo

		// 2. gestionar ese error para que no siga en el programa, ESTO NO!!! DEBE
		// GESTIONARSE EN LA LP

		boolean error = false;

		clsCliente p = new clsCliente(dni, nombre, email, password, idCliente);

		error = clientes.add(p);

		return error;

	}

//=====================================================================================================================================
	/**
	 * 
	 * @throws BDException
	 */
	public void guardarClienteLD() {

		Iterator<itfProperty> it = clientes.iterator();

		itfProperty u;

		while (it.hasNext()) {
			u = it.next();

			String dni = (String) u.getObjectProperty(clsConstantes.DNI);

			String nombre = (String) u.getObjectProperty(clsConstantes.NOMBRE);

			String email = (String) u.getObjectProperty(clsConstantes.EMAIL);

			String password = (String) u.getObjectProperty(clsConstantes.PASSWORD);

			String idCliente = (String) u.getObjectProperty(clsConstantes.ID_CLIENTE);

			gld.insertarClienteBD(dni, nombre, email, password, idCliente);

		}
	}

//=====================================================================================================================================
	/**
	 * 
	 * @param dni
	 */
	// de la LN
	public void borrarCliente(String dni) {

		itfProperty c = new clsVendedor(dni, null, null, null, null);

		System.out.println("MOSTRAR LA TABLA DE VENDEDORES");
		System.out.println(clientes);

		// NO ENCUENTRA TU VIVIENDA POR LO QUE NO LA PUEDE BORRAR
		if (clientes.contains(c))
			System.out.println("He encontrado al cliente!!!");

		boolean b = clientes.remove(c);

		System.out.println("Resultado del borrado");
		System.out.println(b);

		gld.borrarVendedor(dni);
	}

//=====================================================================================================================================0
	/**
	 * 
	 * @param dni
	 * @return
	 */
	// se recupera de la LN no de la LD
	public itfProperty recuperarCliente(String dni) {

		try {
			for (itfProperty c : clientes) {
				String DNIQueEstasMirando = ((String) c.getObjectProperty(clsConstantes.DNI));

				if (DNIQueEstasMirando.equals(dni)) {
					System.out.println("Encontrado Cliente!!!");

					return c;
				}
			}
		} catch (DNIIncorrectoException e) {

			return null; // <---- este null es por si el ID NO es VALIDO!!!!!

		}

		return null;
	}

//======================================================================================================================
	/**
	 * 
	 * @param dni
	 */
	public void recuperarClienteLD(String dni) {
		clsFila x = gld.recuperarCliente(dni);

		String elDniQueRecuperas = (String) x.getData("dni");
		String elNombreQueRecuperas = (String) x.getData("nombre");
		String elEmailQueRecuperas = (String) x.getData("email");
		String elPasswordQueRecuperas = (String) x.getData("password");
		String elIdClienteQueRecuperas = (String) x.getData("idCliente");

		clsVendedor a = new clsVendedor(elDniQueRecuperas, elNombreQueRecuperas, elEmailQueRecuperas,
				elPasswordQueRecuperas, elIdClienteQueRecuperas);

		clientes.add(a);
	}

//==========================================================================================================================
	public void modificarCliente(String dni, String nombre, String email, String password, String idCliente) {
	}

	public Iterator<itfProperty> MostrarClientes() {

		return clientes.iterator();

	}

	/**
	 * se recuperan de la LD
	 */

	public void recuperarClientes() {
		// ir la LD, recuperar la info de los alumnos mediante clsFila y los
		// voy a guardar en la LN como itfProperty, asi podras ordenarlos, etc,
		// etc,etc...
		// porque en la capa LN los datos deben ser itfProperty (NO CASTING) ( SI
		// WRAPING )

		// recuperas desde la LD
		Set<clsFila> filasQueRecuperas = gld.recuperarClientes();

		// declarar las variables fuera del bucle
		String dni;
		String nombre;
		String email;
		String password;
		String idCliente;

		itfProperty v;

		// por cada alumno recuperado de la LD
		for (clsFila f : filasQueRecuperas) {
			// recuperar cada dato de la clsFila de la LD
			dni = (String) f.getData("dni");
			nombre = (String) f.getData("nombre");
			email = (String) f.getData("email");
			password = (String) f.getData("password");
			idCliente = (String) f.getData("idCliente");

			// crear un itfProperty
			v = new clsCliente(dni, nombre, email, password, idCliente);

			// a�adir ese objeto al LN
			clientes.add(v);
		}
	}

	public boolean comprobarUsuario(String strUser, String strPass) {

		return gld.comprobarUsuario(strUser, strPass);
	}

}
