package LN;

import java.util.Set;

import LD.clsFila;
import LD.clsGestorLD;

/**
 * 
 * Clase encargada del calculo del precio medio de las viviendas rurales y
 * urbanas para poder mostrarlo como dato de inter�s al inicio de la aplicaci�n
 * 
 * Declara una serie de posibles m�todos a desarrollar, que se encuentran sin
 * implementar
 * 
 * @author Asier y Natxo
 *
 */
public class clsEstadisticaViviendas {

	private int precioMedioUrbanaMC;
	private int precioMedioRuralMC;

	private int precioMaximoMC;
	private int precioMinimoMC;

	private String idUrbanaPrecioMaximoMC;
	private String idUrbanaPrecioMinimoMC;

	private String idRuralPrecioMaximoMC;
	private String idRuralPrecioMinimoMC;

	private clsGestorLD gld = new clsGestorLD();

	public void calcularPrecioMedioUrbanaMC() {

		Set<clsFila> filasUrbana = gld.getUrbanas();
		int p;
		int m;
		int d;
		int suma = 0;

		for (clsFila f : filasUrbana) {
			p = (Integer) f.getData("precio");
			m = (Integer) f.getData("metros");

			d = p / m;
			suma = suma + d;
		}

		precioMedioUrbanaMC = suma / filasUrbana.size();
	}

	public int getPrecioMedioUrbanaMC() {
		return precioMedioUrbanaMC;
	}

	public void calcularPrecioMedioRuralMC() {

		Set<clsFila> filasRural = gld.getRurales();
		int p;
		int m;
		int d;
		int suma = 0;

		for (clsFila f : filasRural) {
			p = (Integer) f.getData("precio");
			m = (Integer) f.getData("metros");

			d = p / m;
			suma = suma + d;
		}

		precioMedioRuralMC = suma / filasRural.size();
	}

	public int getPrecioMedioRuralMC() {

		return precioMedioRuralMC;
	}

}
