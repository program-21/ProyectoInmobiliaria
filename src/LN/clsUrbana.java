package LN;

import COMUN.clsConstantes;
import COMUN.itfProperty;

/**
 * Esta clase es la encargada de crear distintas vivendas urbanas. Esta clase
 * hereda los atributos y m�todos de su clase padre que es clsVivienda. En
 * a�adidura a estos atributos heredados, adem�s posee otros propios que son
 * trastero y garaje.
 * 
 * @author Industria 4.0 Asier y Natxo
 *
 */
public class clsUrbana extends clsVivienda implements itfProperty {

	@Override
	public String toString() {
		return super.toString() + "clsUrbana [trastero=" + trastero + ", garaje=" + garaje + "]";
	}

	private boolean trastero;
	private boolean garaje;

	public clsUrbana(String idVivienda, String estado, int precio, int metros, int habitaciones, String disponibilidad,
			int precioAlquilerCatalogo, boolean trastero, boolean garaje) {
		super(idVivienda, estado, precio, metros, habitaciones, disponibilidad, precioAlquilerCatalogo);

		this.garaje = garaje;
		this.trastero = trastero;
	}

	public boolean isTrastero() {
		return trastero;
	}

	public void setTrastero(boolean trastero) {
		this.trastero = trastero;
	}

	public boolean isGaraje() {
		return garaje;
	}

	public void setGaraje(boolean garaje) {
		this.garaje = garaje;
	}

	@Override
	public Object getObjectProperty(String propiedad) {
		switch (propiedad) {
		case clsConstantes.GARAJE:
			return garaje;
		case clsConstantes.TRASTERO:
			return trastero;

		default:
			return super.getObjectProperty(propiedad);
		}
	}

}
