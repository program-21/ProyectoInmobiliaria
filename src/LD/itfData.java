package LD;

/**
 * 
 * Interface que implementanlos metodos de la l�gica de datos
 * 
 * @author Asier y Natxo
 */
public interface itfData {
	public void ponerColumna(String columna, Object valor);

	public Object getData(String columna);
}