package LD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import COMUN.ItfData;
import COMUN.clsConstantes;

/**
 * 
 * Clase que gestiona la conexi�n con la base de datos y la inserci�n y recogida
 * de datos de la misma.
 * 
 * @author Asier y Natxo
 */
public class clsGestorLD implements ItfData {

	private Connection conexion = null;
	private Statement st;
	private ResultSet rs;

	private PreparedStatement ps;

//=============================================================CONEXION--BD============================================================
	/**
	 * Conexion con Base de Datos
	 */

	public void conectar() {
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");

			String sURL = "jdbc:mysql://localhost:3306/Inmobiliaria?useSSL=true&serverTimezone=GMT";

			conexion = java.sql.DriverManager.getConnection(sURL, "root", "0000");
			// conexion = java.sql.DriverManager.getConnection(sURL, "root", "root");
			System.out.println("Conexi�n establecida");
		} catch (Exception e) {

			JOptionPane.showMessageDialog(null, "CONEXI�N FALLIDA, REVISA QUE EL SERVICIO HAYA SIDO INICIADO");
		}
	}

	/**
	 * Desconexion con Base de Datos
	 */

	public void desconectar() {
		try {
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
//====================================================INSERTA--URBANA--EN--BD================================0=====================

	/**
	 * Metodos Viviendas Urbanas que se encarga de insertar cada vivienda urbana en
	 * BD
	 */
	/**
	 * 
	 */
	public void insertarUrbana(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo, boolean trastero, boolean garaje) {

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.INSERTAR_URBANA);

			ps.setString(1, idVivienda);
			ps.setString(2, estado);
			ps.setInt(3, precio);
			ps.setInt(4, metros);
			ps.setInt(5, habitaciones);
			ps.setString(6, disponibilidad);
			ps.setInt(7, precioAlquilerCatalogo);
			ps.setBoolean(8, trastero);
			ps.setBoolean(9, garaje);

			ps.executeUpdate();

		} catch (SQLException e) {

		} finally {
			desconectar();
		}

	}

//======================================================INSERTA--RURAL--EN--BD======================================================
	/**
	 * Metodos a�adir Viviendas Rurales a la BD
	 */
	/**
	 * 
	 */
	@Override
	public void insertarRural(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo, int metrosTerreno, boolean granero) {

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.INSERTAR_RURAL);

			ps.setString(1, idVivienda);
			ps.setString(2, estado);
			ps.setInt(3, precio);
			ps.setInt(4, metros);
			ps.setInt(5, habitaciones);
			ps.setString(6, disponibilidad);
			ps.setInt(7, precioAlquilerCatalogo);
			ps.setInt(8, metrosTerreno);
			ps.setBoolean(9, granero);

			ps.executeUpdate();

		} catch (SQLException e) {

		} finally {
			desconectar();
		}

	}

//======================================RECOGE--DE--BD--CADA--FILA--DE--URBANA--EN--CLSFILA==================================0
	/**
	 * Metodo que nos recupera una fila de la tabla de BD que equivale a una
	 * vivienda urbana
	 * 
	 * @return filaUrbana
	 */
	public Set<clsFila> getUrbanas() {

		Set<clsFila> filaUrbana = new HashSet<>();

		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String idVivienda;
		String estado;
		int precio;
		int metros;
		int habitaciones;
		String disponibilidad;
		int precioAlquilerCatalogo;
		boolean trastero;
		boolean garaje;

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_URBANAS);

			rs = ps.executeQuery();

			while (rs.next()) {

				idVivienda = rs.getString(1);
				estado = rs.getString(2);
				precio = rs.getInt(3);
				metros = rs.getInt(4);
				habitaciones = rs.getInt(5);
				disponibilidad = rs.getString(6);
				precioAlquilerCatalogo = rs.getInt(7);
				trastero = rs.getBoolean(8);
				garaje = rs.getBoolean(9);

				// -------------------------------------------------
				clsFila nuevaFila = new clsFila();

				nuevaFila.ponerColumna("idVivienda", idVivienda);
				nuevaFila.ponerColumna("estado", estado);
				nuevaFila.ponerColumna("precio", precio);
				nuevaFila.ponerColumna("metros", metros);
				nuevaFila.ponerColumna("habitaciones", habitaciones);
				nuevaFila.ponerColumna("disponibilidad", disponibilidad);
				nuevaFila.ponerColumna("precioAlquilerCatalogo", precioAlquilerCatalogo);
				nuevaFila.ponerColumna("trastero", trastero);
				nuevaFila.ponerColumna("garaje", garaje);

				filaUrbana.add(nuevaFila);
				// -------------------------------------------------

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return filaUrbana;
	}

//======================================RECOGE--DE--BD--CADA--FILA--DE--RURAL--EN--CLSFILA==================================0
	/**
	 * 
	 * @return
	 */
	public Set<clsFila> getRurales() {

		Set<clsFila> filaRural = new HashSet<clsFila>();

		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String idVivienda;
		String estado;
		int precio;
		int metros;
		int habitaciones;
		String disponibilidad;
		int precioAlquilerCatalogo;
		int metrosTerreno;
		boolean granero;

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_RURALES);

			rs = ps.executeQuery();

			while (rs.next()) {

				idVivienda = rs.getString(1);
				estado = rs.getString(2);
				precio = rs.getInt(3);
				metros = rs.getInt(4);
				habitaciones = rs.getInt(5);
				disponibilidad = rs.getString(6);
				precioAlquilerCatalogo = rs.getInt(7);
				metrosTerreno = rs.getInt(8);
				granero = rs.getBoolean(9);

				// -------------------------------------------------
				clsFila nuevaFila = new clsFila();

				nuevaFila.ponerColumna("idVivienda", idVivienda);
				nuevaFila.ponerColumna("estado", estado);
				nuevaFila.ponerColumna("precio", precio);
				nuevaFila.ponerColumna("metros", metros);
				nuevaFila.ponerColumna("habitaciones", habitaciones);
				nuevaFila.ponerColumna("disponibilidad", disponibilidad);
				nuevaFila.ponerColumna("precioAlquilerCatalogo", precioAlquilerCatalogo);
				nuevaFila.ponerColumna("metrosTerreno", metrosTerreno);
				nuevaFila.ponerColumna("granero", granero);

				filaRural.add(nuevaFila);
				// -------------------------------------------------

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return filaRural;
	}

//===============================================ACTUALIZAR--URBANA===================================================			
	/**
	 * Este m�todo se encarga de actualizar los campos de una vivienda urbana que ya
	 * exista en la BD
	 */
	@Override
	public void actualizarUrbana(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo, boolean trastero, boolean garaje) {

		conectar();

		try {
			st = conexion.createStatement();

			ps = conexion.prepareStatement(clsConstantes.ACTUALIZAR_URBANA);

			ps.setString(1, estado);
			ps.setInt(2, precio);
			ps.setInt(3, metros);
			ps.setInt(4, habitaciones);
			ps.setString(5, disponibilidad);
			ps.setInt(6, precioAlquilerCatalogo);
			ps.setBoolean(7, trastero);
			ps.setBoolean(8, garaje);
			ps.setString(9, idVivienda);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}
	}

//==========================================ACTUALIZAR--RURAL==============================================
	/**
	 * 
	 */
	@Override
	public void actualizarRural(String idVivienda, String estado, int precio, int metros, int habitaciones,
			String disponibilidad, int precioAlquilerCatalogo, int metrosTerreno, boolean granero) {

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.ACTUALIZAR_RURAL);

			ps.setString(1, estado);
			ps.setInt(2, precio);
			ps.setInt(3, metros);
			ps.setInt(4, habitaciones);
			ps.setString(5, disponibilidad);
			ps.setInt(6, precioAlquilerCatalogo);
			ps.setInt(7, metrosTerreno);
			ps.setBoolean(8, granero);
			ps.setString(9, idVivienda);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

	}

//======================================RECUPERAR--URBANA--POR--ID=================================================================
	/**
	 * Metodo que se encarga de recuperar una vivienda por el ID introducido por el
	 * usuario
	 * 
	 * @param id
	 * @return
	 */
	public clsFila recuperarUrbana(String id) {

		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String idVivienda;
		String estado;
		int precio;
		int metros;
		int habitaciones;
		String disponibilidad;
		int precioAlquilerCatalogo;
		boolean trastero;
		boolean garaje;

		clsFila unaFilaDeLaBD = null;

		conectar();

		try {
			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_URBANA);

			ps.setString(1, id);

			rs = ps.executeQuery();

			while (rs.next()) {

				idVivienda = rs.getString(1);
				estado = rs.getString(2);
				precio = rs.getInt(3);
				metros = rs.getInt(4);
				habitaciones = rs.getInt(5);
				disponibilidad = rs.getString(6);
				precioAlquilerCatalogo = rs.getInt(7);
				trastero = rs.getBoolean(8);
				garaje = rs.getBoolean(9);

				unaFilaDeLaBD = new clsFila();

				unaFilaDeLaBD.ponerColumna("idViviendaUrbana", idVivienda);
				unaFilaDeLaBD.ponerColumna("estado", estado);
				unaFilaDeLaBD.ponerColumna("precio", precio);
				unaFilaDeLaBD.ponerColumna("metros", metros);
				unaFilaDeLaBD.ponerColumna("habitaciones", habitaciones);
				unaFilaDeLaBD.ponerColumna("disponibilidad", disponibilidad);
				unaFilaDeLaBD.ponerColumna("precioAlquilerCatalogo", precioAlquilerCatalogo);
				unaFilaDeLaBD.ponerColumna("trastero", trastero);
				unaFilaDeLaBD.ponerColumna("garaje", garaje);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return unaFilaDeLaBD;
	}

//===============================================RECUPERAR--RURAL--DE--BD--POR--ID================================================
	/**
	 * Metodo que se encarga de recuperar una vivienda rural por el ID introducido
	 * por el usuario
	 * 
	 * @param id
	 * @return
	 */
	public clsFila recuperarRural(String id) {

		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String idVivienda;
		String estado;
		int precio;
		int metros;
		int habitaciones;
		String disponibilidad;
		int precioAlquilerCatalogo;
		int metrosTerreno;
		boolean granero;

		clsFila unaFilaDeLaBD = null;

		conectar();

		try {
			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_RURAL);

			ps.setString(1, id);

			rs = ps.executeQuery();

			while (rs.next()) {

				idVivienda = rs.getString(1);
				estado = rs.getString(2);
				precio = rs.getInt(3);
				metros = rs.getInt(4);
				habitaciones = rs.getInt(5);
				disponibilidad = rs.getString(6);
				precioAlquilerCatalogo = rs.getInt(7);
				metrosTerreno = rs.getInt(8);
				granero = rs.getBoolean(9);

				unaFilaDeLaBD = new clsFila();

				unaFilaDeLaBD.ponerColumna("idViviendaRural", idVivienda);
				unaFilaDeLaBD.ponerColumna("estado", estado);
				unaFilaDeLaBD.ponerColumna("precio", precio);
				unaFilaDeLaBD.ponerColumna("metros", metros);
				unaFilaDeLaBD.ponerColumna("habitaciones", habitaciones);
				unaFilaDeLaBD.ponerColumna("disponibilidad", disponibilidad);
				unaFilaDeLaBD.ponerColumna("precioAlquilerCatalogo", precioAlquilerCatalogo);
				unaFilaDeLaBD.ponerColumna("metrosTerreno", metrosTerreno);
				unaFilaDeLaBD.ponerColumna("granero", granero);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return unaFilaDeLaBD;
	}

//==================================BORRA--VIVIENDA--URBANA--DE--BD==========================================
	/**
	 * 	
	 */
	@Override
	public void borrarUrbana(String idVivienda) {

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.BORRAR_URBANA);

			ps.setString(1, idVivienda);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}
	}

//=================================================BORRAR--RURAL--DE--BD--POR--ID==============================================0===
	/**
	 * 
	 */
	@Override
	public void borrarRural(String idVivienda) {

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.BORRAR_RURAL);

			ps.setString(1, idVivienda);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

	}

//=======================================RECUPERA======VENDEDOR===POR=====DNI==========================================
	public clsFila recuperarVendedor(String dniArecuperar) {
		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String dni;
		String nombre;
		String email;
		String password;
		String idVendedor;

		clsFila unaFilaDeLaBD = null;

		conectar();

		try {
			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_VENDEDOR);

			ps.setString(1, dniArecuperar);

			rs = ps.executeQuery();

			while (rs.next()) {

				dni = rs.getString(1);
				nombre = rs.getString(2);
				email = rs.getString(3);
				password = rs.getString(4);
				idVendedor = rs.getString(5);

				unaFilaDeLaBD = new clsFila();

				unaFilaDeLaBD.ponerColumna("dni", dni);
				unaFilaDeLaBD.ponerColumna("nombre", nombre);
				unaFilaDeLaBD.ponerColumna("email", email);
				unaFilaDeLaBD.ponerColumna("password", password);
				unaFilaDeLaBD.ponerColumna("idVendedor", idVendedor);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return unaFilaDeLaBD;
	}
//=======================================RECUPERA==TODOS==LOS=CAMPOS==DE==VENDEDORES=============================================

	/**
	 * M�todo que recupera todos los atributos de cada vendedor y los retorna en
	 * forma de clsFila "filas"
	 * 
	 * @return
	 */
	public Set<clsFila> recuperarVendedores() {

		Set<clsFila> filaVendedor = new HashSet<clsFila>();
		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String dni;
		String nombre;
		String email;
		String password;
		String idVendedor;

		conectar();
		try {

			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_VENDEDORES);

			rs = ps.executeQuery();

			while (rs.next()) {

				dni = rs.getString(1);
				nombre = rs.getString(2);
				email = rs.getString(3);
				password = rs.getString(4);
				idVendedor = rs.getString(5);

				// -------------------------------------------------
				clsFila nuevaFila = new clsFila();

				nuevaFila.ponerColumna("dni", dni);
				nuevaFila.ponerColumna("nombre", nombre);
				nuevaFila.ponerColumna("email", email);
				nuevaFila.ponerColumna("password", password);
				nuevaFila.ponerColumna("idVendedor", idVendedor);

				filaVendedor.add(nuevaFila);
				// -------------------------------------------------

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return filaVendedor;
	}

//============================================BORRA======VENDEDOR=====DNI==========================================================0
	@Override
	public void borrarVendedor(String dni) {
		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.BORRAR_VENDEDOR);

			ps.setString(1, dni);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

	}

//==========================================INSERTA=============VENDEDOR=========EN==============BD================================	
	/**
	 * 
	 */
	@Override
	public void insertarVendedorBD(String dni, String nombre, String email, String password, String idVendedor) {

		conectar();
		try {

			ps = conexion.prepareStatement(clsConstantes.INSERTAR_VENDEDOR);

			ps.setString(1, dni);
			ps.setString(2, nombre);
			ps.setString(3, email);
			ps.setString(4, password);
			ps.setString(5, idVendedor);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			desconectar();
		}

	}

	@Override
	public void actualizarVendedor(String dni, String nombre, String email, String password, String idVendedor) {
		// TODO Auto-generated method stub

	}
//==================================================CLIENTE==========================================================
//=======================================RECUPERA======CLIENTE===POR=====DNI==========================================

	/**
	 * 
	 * @param dniArecuperar
	 * @return
	 */
	public clsFila recuperarCliente(String dniArecuperar) {
		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String dni;
		String nombre;
		String email;
		String password;
		String idCliente;

		clsFila unaFilaDeLaBD = null;

		conectar();

		try {
			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_CLIENTE);

			ps.setString(1, dniArecuperar);

			rs = ps.executeQuery();

			while (rs.next()) {

				dni = rs.getString(1);
				nombre = rs.getString(2);
				email = rs.getString(3);
				password = rs.getString(4);
				idCliente = rs.getString(5);

				unaFilaDeLaBD = new clsFila();

				unaFilaDeLaBD.ponerColumna("dni", dni);
				unaFilaDeLaBD.ponerColumna("nombre", nombre);
				unaFilaDeLaBD.ponerColumna("email", email);
				unaFilaDeLaBD.ponerColumna("password", password);
				unaFilaDeLaBD.ponerColumna("idCliente", idCliente);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return unaFilaDeLaBD;
	}

//=======================================RECUPERA==TODOS==LOS==CAMPOS===DE==CLIENTE==========================================	
	/**
	 * M�todo que recupera todos los atributos de cada cliente y los retorna en
	 * forma de clsFila "filas"
	 * 
	 * @return
	 */
	public Set<clsFila> recuperarClientes() {

		Set<clsFila> filaCliente = new HashSet<clsFila>();
		// se crean las variables fuera del bucle poe eficiencia as� no crea nuevas cada
		// vez que recupera una fila y lo que hace es
		// machacar las que ya existen
		String dni;
		String nombre;
		String email;
		String password;
		String idCliente;

		conectar();
		try {

			ps = conexion.prepareStatement(clsConstantes.RECUPERAR_CLIENTES);

			rs = ps.executeQuery();

			while (rs.next()) {

				dni = rs.getString(1);
				nombre = rs.getString(2);
				email = rs.getString(3);
				password = rs.getString(4);
				idCliente = rs.getString(5);

				// -------------------------------------------------
				clsFila nuevaFila = new clsFila();

				nuevaFila.ponerColumna("dni", dni);
				nuevaFila.ponerColumna("nombre", nombre);
				nuevaFila.ponerColumna("email", email);
				nuevaFila.ponerColumna("password", password);
				nuevaFila.ponerColumna("idCliente", idCliente);

				filaCliente.add(nuevaFila);
				// -------------------------------------------------

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return filaCliente;
	}
	// ============================================BORRA======CLIENTE=====DNI==========================================================0

	@Override
	public void borrarCliente(String dni) {
		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.BORRAR_CLIENTE);

			ps.setString(1, dni);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

	}
//==========================================INSERTA=======CLIENTE=========EN==============BD================================	

	@Override
	public void insertarClienteBD(String dni, String nombre, String email, String password, String idCliente) {

		conectar();
		try {

			ps = conexion.prepareStatement(clsConstantes.INSERTAR_CLIENTE);

			ps.setString(1, dni);
			ps.setString(2, nombre);
			ps.setString(3, email);
			ps.setString(4, password);
			ps.setString(5, idCliente);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			desconectar();
		}

	}

	@Override
	public void actualizarCliente(String dni, String nombre, String email, String password, String idCliente) {
		// TODO Auto-generated method stub

	}

	public boolean comprobarVendedor(String strUser, String strPass) {
		boolean correcto = false;

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.CONSULTAR_NOMBRE_PASSWORD_VENDEDOR);

			ps.setString(1, strUser);

			ps.setString(2, strPass);

			rs = ps.executeQuery();

			// hay una fila?????
			if (rs.next()) {

				correcto = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return correcto;
	}

	public boolean comprobarCliente(String strUser, String strPass) {
		boolean correcto = false;

		conectar();

		try {

			ps = conexion.prepareStatement(clsConstantes.CONSULTAR_NOMBRE_PASSWORD_CLIENTE);

			ps.setString(1, strUser);

			ps.setString(2, strPass);

			rs = ps.executeQuery();

			// hay una fila?????
			if (rs.next()) {

				correcto = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			desconectar();
		}

		return correcto;
	}

	public boolean comprobarUsuario(String strUser, String strPass) {
		if (strUser.equalsIgnoreCase("admin")) {

			return comprobarVendedor(strUser, strPass);

		} else {
			return comprobarCliente(strUser, strPass);
		}

	}

}
