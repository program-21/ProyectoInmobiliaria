package LD;

/**
 * 
 * Clase que genera la excepcion de BD
 * 
 * @author Asier y Natxo
 *
 */
public class BDException extends Exception {

	public BDException(String mensaje) {
		// TODO Auto-generated constructor stub
		super(mensaje);
	}

}