package LD;

import java.util.HashMap;

import COMUN.ItfData;

/**
 * 
 * Clase Necesaria para la recogida de datos desde BD mediante la estructura de
 * filas
 * 
 * @author Asier y Natxo
 */
public class clsFila implements itfData {

	private HashMap<String, Object> fila;

	public clsFila() {
		fila = new HashMap<String, Object>();
	}

	public void ponerColumna(String columna, Object valor) {
		fila.put(columna, valor);
	}

	public Object getData(String columna) {
		return fila.get(columna);
	}

}