-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Inmobiliaria
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Inmobiliaria` ;

-- -----------------------------------------------------
-- Schema Inmobiliaria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Inmobiliaria` DEFAULT CHARACTER SET utf8 ;
USE `Inmobiliaria` ;

-- -----------------------------------------------------
-- Table `Inmobiliaria`.`ViviendaRural`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`ViviendaRural` (
  `idViviendaRural` VARCHAR(9) NOT NULL,
  `Estado` VARCHAR(45) NOT NULL,
  `Precio` INT NULL,
  `Metros` INT NOT NULL,
  `Habitaciones` INT NOT NULL,
  `Disponibilidad` VARCHAR(12) NOT NULL,
  `PrecioAlquilerCatalogo` DECIMAL(8) NULL,
  `MetrosTerreno` INT NULL,
  `Granero` TINYINT NOT NULL,
  PRIMARY KEY (`idViviendaRural`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`ViviendaUrbana`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`ViviendaUrbana` (
  `idViviendaUrbana` VARCHAR(9) NOT NULL,
  `Estado` VARCHAR(45) NOT NULL,
  `Precio` INT NULL,
  `Metros` INT NOT NULL,
  `Habitaciones` INT NOT NULL,
  `Disponibilidad` VARCHAR(12) NOT NULL,
  `PrecioAlquilerCatalogo` DECIMAL(8) NULL,
  `Trastero` TINYINT NOT NULL,
  `Garaje` TINYINT NOT NULL,
  PRIMARY KEY (`idViviendaUrbana`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`Vendedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`Vendedor` (
  `idVendedor` INT NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `Apellidos` VARCHAR(45) NOT NULL,
  `DNI` VARCHAR(45) NOT NULL,
  `e-mail` VARCHAR(45) NULL,
  `movil` INT NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idVendedor`),
  UNIQUE INDEX `DNI_UNIQUE` (`DNI` ASC) VISIBLE,
  UNIQUE INDEX `password_UNIQUE` (`password` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`Clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`Clientes` (
  `DNI` VARCHAR(9) NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `Apellidos` VARCHAR(45) NOT NULL,
  `e-mail` VARCHAR(45) NOT NULL,
  `movil` INT NULL,
  PRIMARY KEY (`DNI`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`TipoTransaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`TipoTransaccion` (
  `idTipoTransaccion` VARCHAR(5) NOT NULL,
  `Descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idTipoTransaccion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`Transacciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`Transacciones` (
  `idTransaccion` INT NOT NULL,
  `fechaTrans` DATE NULL,
  `Clientes_DNI` VARCHAR(9) NOT NULL,
  `Vendedor_idVendedor` INT NOT NULL,
  `TipoTransaccion_idTipoTransaccion` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`idTransaccion`, `Clientes_DNI`, `Vendedor_idVendedor`, `TipoTransaccion_idTipoTransaccion`),
  INDEX `fk_Transacciones_Clientes1_idx` (`Clientes_DNI` ASC) VISIBLE,
  INDEX `fk_Transacciones_Vendedor1_idx` (`Vendedor_idVendedor` ASC) VISIBLE,
  INDEX `fk_Transacciones_TipoTransacciones1_idx` (`TipoTransaccion_idTipoTransaccion` ASC) VISIBLE,
  CONSTRAINT `fk_Transacciones_Clientes1`
    FOREIGN KEY (`Clientes_DNI`)
    REFERENCES `Inmobiliaria`.`Clientes` (`DNI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transacciones_Vendedor1`
    FOREIGN KEY (`Vendedor_idVendedor`)
    REFERENCES `Inmobiliaria`.`Vendedor` (`idVendedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transacciones_TipoTransacciones1`
    FOREIGN KEY (`TipoTransaccion_idTipoTransaccion`)
    REFERENCES `Inmobiliaria`.`TipoTransaccion` (`idTipoTransaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`GestionAlquiler`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`GestionAlquiler` (
  `FechaInicio` DATE NOT NULL,
  `FechaFin` DATE NULL,
  `Transaccion_idTransaccion` INT NOT NULL,
  `PrecioAlquilerHistorico` VARCHAR(45) NULL,
  PRIMARY KEY (`Transaccion_idTransaccion`),
  CONSTRAINT `fk_GestionAlquiler_Transacciones1`
    FOREIGN KEY (`Transaccion_idTransaccion`)
    REFERENCES `Inmobiliaria`.`Transacciones` (`idTransaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`Transacciones_ViviendaUrbana`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`Transacciones_ViviendaUrbana` (
  `Transaccion_idTransaccion` INT NOT NULL,
  `ViviendaUrbana_idViviendaUrbana` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`Transaccion_idTransaccion`, `ViviendaUrbana_idViviendaUrbana`),
  INDEX `fk_Transacciones_has_ViviendaUrbana_ViviendaUrbana1_idx` (`ViviendaUrbana_idViviendaUrbana` ASC) VISIBLE,
  INDEX `fk_Transacciones_has_ViviendaUrbana_Transacciones1_idx` (`Transaccion_idTransaccion` ASC) VISIBLE,
  CONSTRAINT `fk_Transacciones_has_ViviendaUrbana_Transacciones1`
    FOREIGN KEY (`Transaccion_idTransaccion`)
    REFERENCES `Inmobiliaria`.`Transacciones` (`idTransaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transacciones_has_ViviendaUrbana_ViviendaUrbana1`
    FOREIGN KEY (`ViviendaUrbana_idViviendaUrbana`)
    REFERENCES `Inmobiliaria`.`ViviendaUrbana` (`idViviendaUrbana`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Inmobiliaria`.`Transacciones_ViviendaRural`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Inmobiliaria`.`Transacciones_ViviendaRural` (
  `Transaccion_idTransaccion` INT NOT NULL,
  `ViviendaRural_idViviendaRural` VARCHAR(9) NOT NULL,
  PRIMARY KEY (`Transaccion_idTransaccion`, `ViviendaRural_idViviendaRural`),
  INDEX `fk_Transacciones_has_ViviendaRural_ViviendaRural1_idx` (`ViviendaRural_idViviendaRural` ASC) VISIBLE,
  INDEX `fk_Transacciones_has_ViviendaRural_Transacciones1_idx` (`Transaccion_idTransaccion` ASC) VISIBLE,
  CONSTRAINT `fk_Transacciones_has_ViviendaRural_Transacciones1`
    FOREIGN KEY (`Transaccion_idTransaccion`)
    REFERENCES `Inmobiliaria`.`Transacciones` (`idTransaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transacciones_has_ViviendaRural_ViviendaRural1`
    FOREIGN KEY (`ViviendaRural_idViviendaRural`)
    REFERENCES `Inmobiliaria`.`ViviendaRural` (`idViviendaRural`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
