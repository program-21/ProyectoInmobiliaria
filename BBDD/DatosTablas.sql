CREATE DATABASE  IF NOT EXISTS `inmobiliaria` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `inmobiliaria`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: inmobiliaria
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cliente` (
  `dni` varchar(9) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `idCliente` varchar(45) NOT NULL,
  PRIMARY KEY (`idCliente`),
  UNIQUE KEY `dni_UNIQUE` (`dni`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES ('88888888Q','borja','borja','borja','borja'),('55474625R','jo','jo@gmail.com','jo','jo'),('00000000I','marisa','marisa@gmail.com','marisa','marisa'),('99999999Z','nuria','nuria@gmail.com','nuria','nuria'),('44444444R','oo','oo','oo','oo');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipotransaccion`
--

DROP TABLE IF EXISTS `tipotransaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipotransaccion` (
  `idTipoTransaccion` varchar(5) NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idTipoTransaccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipotransaccion`
--

LOCK TABLES `tipotransaccion` WRITE;
/*!40000 ALTER TABLE `tipotransaccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipotransaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendedor`
--

DROP TABLE IF EXISTS `vendedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `vendedor` (
  `dni` varchar(45) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `idVendedor` varchar(45) NOT NULL,
  PRIMARY KEY (`idVendedor`),
  UNIQUE KEY `DNI_UNIQUE` (`dni`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendedor`
--

LOCK TABLES `vendedor` WRITE;
/*!40000 ALTER TABLE `vendedor` DISABLE KEYS */;
INSERT INTO `vendedor` VALUES ('11111111A','admin','admin@gmail.com','admin','admin'),('11111111B','b','b','b','b'),('54545546H','paco','paco','paco','paco'),('44683502E','na','na','v1','v1');
/*!40000 ALTER TABLE `vendedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viviendarural`
--

DROP TABLE IF EXISTS `viviendarural`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `viviendarural` (
  `idViviendaRural` varchar(9) NOT NULL,
  `Estado` varchar(45) NOT NULL,
  `Precio` int(11) DEFAULT NULL,
  `Metros` int(11) NOT NULL,
  `Habitaciones` int(11) NOT NULL,
  `Disponibilidad` varchar(12) NOT NULL,
  `PrecioAlquilerCatalogo` decimal(8,0) DEFAULT NULL,
  `MetrosTerreno` int(11) DEFAULT NULL,
  `Granero` tinyint(4) NOT NULL,
  PRIMARY KEY (`idViviendaRural`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viviendarural`
--

LOCK TABLES `viviendarural` WRITE;
/*!40000 ALTER TABLE `viviendarural` DISABLE KEYS */;
INSERT INTO `viviendarural` VALUES ('1','1',1,1,1,'1',1,1,1),('9','9',9,9,9,'9',9,9,0),('r102','venta',230000,160,6,'abril',0,1000,0),('r220','venta',380000,180,5,'julio',0,2000,1);
/*!40000 ALTER TABLE `viviendarural` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viviendaurbana`
--

DROP TABLE IF EXISTS `viviendaurbana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `viviendaurbana` (
  `idViviendaUrbana` varchar(9) NOT NULL,
  `Estado` varchar(45) NOT NULL,
  `Precio` int(11) DEFAULT NULL,
  `Metros` int(11) NOT NULL,
  `Habitaciones` int(11) NOT NULL,
  `Disponibilidad` varchar(12) NOT NULL,
  `PrecioAlquilerCatalogo` decimal(8,0) DEFAULT NULL,
  `Trastero` tinyint(4) NOT NULL,
  `Garaje` tinyint(4) NOT NULL,
  PRIMARY KEY (`idViviendaUrbana`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viviendaurbana`
--

LOCK TABLES `viviendaurbana` WRITE;
/*!40000 ALTER TABLE `viviendaurbana` DISABLE KEYS */;
INSERT INTO `viviendaurbana` VALUES ('11','11',11,11,11,'11',11,1,1),('u100','venta',190000,90,2,'septiembre',700,1,1),('u101','alquiler',0,87,3,'agosto',600,1,0),('u102','venta',111500,70,2,'Agosto',0,0,0),('u106','venta',280000,130,4,'septiembre',0,1,1),('u231','alquiler',0,90,3,'julio',700,0,0),('u707','venta',120000,65,2,'marzo',0,1,0);
/*!40000 ALTER TABLE `viviendaurbana` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-27 22:17:22
